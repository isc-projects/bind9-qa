#!/usr/bin/env python3
"""
Post-merge MR metadata adjustment

Inputs:
- env vars: BIND_TEAM_WRITE_TOKEN + CI_SERVER_URL
  - Token must allow MR metadata manipulation
- CLI: Gitlab Project ID + MR ID
  - reads labels from the MR and first line of MR text

Outputs:
    - set MR milestone to Not released yet
    - adjust No CHANGES and Release Notes label to match MR text
"""

import argparse
import logging
import os
import re

import gitlab


def not_released_yet(proj, mr):
    """Set MR milestone to 'Not released yet'"""
    assert proj.id == mr.project_id, f"{proj.id} != {mr.project_id}"
    milestones = proj.milestones.list(search="Not released yet")
    assert len(milestones) == 1, "unexpected {milestones}"
    mr.milestone_id = milestones[0].id
    mr.save()


def expected_doc_labels(mr):
    """
    Determine if 'No CHANGES' and 'Release Notes' labels should be present
    or not on the given MR.
    """
    mr_title_re = re.compile(
        r"^(\[9\.[0-9]{2}(-S)?\])?\s*(\[[^]]*\]\s*)?((chg|fix|new|rem|sec):)?\s*((dev|usr|pkg|doc|test)\s*:)?\s*([^\n]*)$",
    )
    mr_title_match = mr_title_re.match(mr.title)
    mr_title_audience = mr_title_match.group(7) if mr_title_match else None
    changes_modified = mr_title_audience in ["usr", "pkg", "dev"]
    release_notes_changed = mr_title_audience in ["usr", "pkg"]

    return {"No CHANGES": not changes_modified, "Release Notes": release_notes_changed}


def adjust_labels(mr, target_state):
    """
    Ensure that labels are not/present on the given MR.
    Input: {'label': bool}
    True = present, False = not present
    """
    for label, present in target_state.items():
        if not present:
            try:
                mr.labels.remove(label)
            except ValueError:
                pass
            else:
                mr.save()
        else:  # present
            if label not in mr.labels:
                mr.labels.append(label)
                mr.save()


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        "projectid",
        type=int,
        help="Gitlab project ID to read MR from",
    )
    parser.add_argument("mrid", type=int, help="MR #")
    args = parser.parse_args()
    logging.info("source MR %s", args)

    server_url = os.environ["CI_SERVER_URL"]
    access_token = os.environ["BIND_TEAM_WRITE_TOKEN"]
    gl = gitlab.Gitlab(url=server_url, private_token=access_token)
    proj = gl.projects.get(args.projectid)

    logging.info("target project: %s", proj.path_with_namespace)
    mr = proj.mergerequests.get(args.mrid)
    logging.info("MR !%d %s", mr.iid, mr.title)

    # if mr.state != "merged":
    #    raise ValueError("MR is not merged yet, merge it first")

    logging.info("going to set milestone to 'Not released yet'")
    not_released_yet(proj, mr)

    logging.info("current MR labels: %s", mr.labels)
    labels_wanted = expected_doc_labels(mr)
    logging.info("labels to be (un)set: %s", labels_wanted)
    adjust_labels(mr, labels_wanted)


if __name__ == "__main__":
    main()
