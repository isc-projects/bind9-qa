#!/usr/bin/python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Setup a new repository in isc-customers and generate a deploy token.
"""

import argparse
import logging
import sys
from typing import List, Tuple
import yaml

import gitlab

from bindutil.bindversion import label2branchmap
from bindutil.environment import get_required_environment_variables
from bindutil.gitlab import get_isc_gitlab
from bindutil.messages import message_to_mattermost, template_to_message


def parse_customer_and_versions(args) -> Tuple[str, List[str]]:
    entitlements = yaml.safe_load(args.entitlements)
    assert "customers" in entitlements, "Invalid entitlements file format"
    customers = entitlements["customers"]
    try:
        customer = customers[args.customer]
    except KeyError:
        logging.error("Customer `%s` not found in entitlements file.", args.customer)
        sys.exit(1)
    try:
        versions = customer["bind9_versions"]
    except KeyError:
        logging.error(
            "Missing `bind9_versions` key in customer entry in entitlements.yaml file."
        )
        sys.exit(1)
    for version in versions:
        if version not in label2branchmap:
            logging.error("Uknown version `%s`. Use proper label name.", version)
            sys.exit(1)
    logging.info("Customer `%s` is entitled to the following versions:", args.customer)
    for version in versions:
        logging.info("  %s", version)
    return args.customer, versions


def create_customer_subgroup_in_namespace(gl, customer: str):
    isc_customers = gl.groups.get("isc-customers")
    subgroup_data = {
        "name": customer,
        "path": customer,
        "visibility": "private",
        "parent_id": isc_customers.id,
    }
    logging.info("Creating `isc-customers/%s` subgroup...", customer)
    try:
        subgroup = gl.groups.create(subgroup_data)
    except gitlab.exceptions.GitlabCreateError as exc:
        logging.error(exc)
        sys.exit(1)
    logging.debug(subgroup)
    logging.info("Gitlab subgroup created: %s", subgroup.web_url)
    return subgroup


def create_customer_bind_project(gl, subgroup):
    project_data = {
        "path": "bind9",
        "namespace_id": subgroup.id,
        "visibility": "private",
        "initialize_with_readme": False,
        "repository_access_level": "enabled",
        "analytics_access_level": "disabled",
        "builds_access_level": "disabled",
        "container_registry_access_level": "disabled",
        "environments_access_level": "disabled",
        "feature_flags_access_level": "disabled",
        "forking_access_level": "disabled",
        "infrastructure_access_level": "disabled",
        "issues_access_level": "disabled",
        "merge_requests_access_level": "disabled",
        "model_experiments_access_level": "disabled",
        "model_registry_access_level": "disabled",
        "monitor_access_level": "disabled",
        "pages_access_level": "disabled",
        "releases_access_level": "disabled",
        "requirements_access_level": "disabled",
        "security_and_compliance_access_level": "disabled",
        "snippets_access_level": "disabled",
        "wiki_access_level": "disabled",
        "lfs_enabled": False,
        "packages_enabled": False,
    }
    try:
        project = gl.projects.create(project_data)
    except gitlab.exceptions.GitlabCreateError as exc:
        logging.error(exc)
        sys.exit(1)
    logging.debug(project)
    logging.info("Customer project created: %s", project.web_url)
    return project


def create_customer_deploy_token(project, customer):
    token_data = {
        "name": customer,
        "username": customer,
        "scopes": [
            "read_repository",
        ],
    }
    try:
        token = project.deploytokens.create(token_data)
    except gitlab.exceptions.GitlabCreateError as exc:
        logging.error(exc)
        sys.exit(1)
    logging.debug(token)
    logging.info("Deploy token created: %s", token.token)
    return token


def get_customer_url(project, token):
    return f"https://{token.username}:{token.token}@gitlab.isc.org/{project.path_with_namespace}.git"


def setup_mattermost_integration(project, webhook, channel):
    integration = project.integrations.get("mattermost", lazy=True)
    integration.webhook = webhook
    integration.username = "gitlab"
    integration.branches_to_be_notified = "all"
    integration.push_events = True
    integration.push_channel = channel
    integration.tag_push_events = True
    integration.tag_push_channel = channel
    integration.issues_events = False
    integration.confidential_issues_events = False
    integration.merge_requests_events = False
    integration.note_events = False
    integration.confidential_note_events = False
    integration.pipeline_events = False
    integration.wiki_page_events = False
    integration.save()
    logging.info("Set up mattermost notification for push events")


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    env = get_required_environment_variables(["mattermost_webhook_url"])
    if not env:
        sys.exit(1)

    parser = argparse.ArgumentParser(description="Setup a new customer git repository")
    parser.add_argument(
        "--entitlements",
        type=argparse.FileType(),
        required=True,
        help="path to entitlements.yaml",
    )
    parser.add_argument("customer", type=str, help="customer's alias")
    parser.add_argument(
        "--channel", default="bind-9-automation", type=str, help="mattermost channel"
    )

    args = parser.parse_args()
    customer, versions = parse_customer_and_versions(args)
    gl = get_isc_gitlab()
    subgroup = create_customer_subgroup_in_namespace(gl, customer)
    project = create_customer_bind_project(gl, subgroup)
    token = create_customer_deploy_token(project, customer)
    url = get_customer_url(project, token)
    logging.info("Customer git-access URL: %s", url)
    setup_mattermost_integration(project, env["mattermost_webhook_url"], args.channel)

    message = template_to_message(
        "customer_repository_created.j2",
        {
            "customer": customer,
            "deploy_token": token.token,
            "git_access_url": url,
            "versions": versions,
        },
    )
    message_to_mattermost(message, args.channel)
    logging.info("Notified `%s` mattermost channel", args.channel)


if __name__ == "__main__":
    main()
