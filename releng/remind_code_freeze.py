#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Generate reminder about upcoming code freeze.
"""

import argparse
import datetime
import logging

from bindutil.gitlab import get_bind9_project
from bindutil.messages import template_to_message, message_to_mattermost
from bindutil.metadata import ReleaseMetadata


def format_time_delta(delta):
    if delta.days >= 7 and delta.days % 7 == 0:
        weeks = delta.days // 7
        return f"{weeks} week{'s' if weeks > 1 else ''}"
    return str(delta).replace(", 0:00:00", "")


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Generate reminder message about upcoming code freeze"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    parser.add_argument(
        "--channel", default="bind9", type=str, help="mattermost channel"
    )
    parser.add_argument(
        "--message-mattermost",
        action="store_true",
        help="set to send message to mattermost",
    )
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata).data
    today = datetime.date.today()
    proj_pub = get_bind9_project("public")

    if (metadata["schedule"]["freeze"] - today).days > 0:
        # before scheduled code freeze date -> remind about this code freeze
        due_date = metadata["schedule"]["freeze"]
    else:
        # after code freeze, but still with old metadata -> remind about the next freeze
        if (metadata["schedule"]["public"] - today).days < 0:
            logging.warning(
                "metadata.json should be updated with current release plans"
            )
        milestone = proj_pub.milestones.get(
            id=metadata["milestones"]["next"]["public"]["id"]
        )
        due_date = datetime.date.fromisoformat(milestone.due_date)
    assert due_date >= today, "code freeze is in the past!"

    data = {
        "month": due_date.strftime("%B"),
        "time_left": format_time_delta(due_date - today),
        "due_date": due_date,
    }
    message = template_to_message("code_freeze_reminder.md.j2", data)
    if args.message_mattermost:
        message_to_mattermost(message, args.channel)
        logging.info(
            "code freeze reminder sent to mattermost channel: %s", args.channel
        )
    else:
        logging.info("use --message-mattermost to send message to mattermost")
        print(message)


if __name__ == "__main__":
    main()
