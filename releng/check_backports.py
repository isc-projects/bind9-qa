#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Automated checker for backports:
It searches for original commit ID in commit messages on the target branch.
Use --log INFO --repo <path to local repo> to understand what it is doing.
"""

import argparse
import logging
import sys

import pygit2

from bindutil.bindversion import label2branchmap, is_same_branch, target_branches
from bindutil.gitlab import get_bind9_project


REPO_FS_PATH = "/home/pspacek/w/pkg/bind/git"


class ConditionalLogHeader:
    """
    Mimicks logging module interface, but buffers "header text"
    until another message with sufficiently high severity level triggers output.
    Parent link can be used to chain these conditional loggers.
    """

    def __init__(self, header: str, level, parent=None):
        self.header = header
        self.header_logged = False
        self.parent = parent
        if parent:
            parent.maybe_log_header(level)
        self.maybe_log_header(level)

    def maybe_log_header(self, level):
        """Log only own header at a given severity level"""
        if not logging.root.isEnabledFor(level):
            return
        if not self.header_logged:
            logging.log(level, self.header)
            self.header_logged = True

    def log(self, level, msg, *args, **kwargs):
        if not logging.root.isEnabledFor(level):
            return
        if self.parent:
            self.parent.maybe_log_header(level)
        self.maybe_log_header(level)
        logging.log(level, msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        return self.log(logging.INFO, msg, *args, *kwargs)

    def warning(self, msg, *args, **kwargs):
        return self.log(logging.WARNING, msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        return self.log(logging.ERROR, msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        return self.log(logging.CRITICAL, msg, *args, **kwargs)


def backport_to_branches(target_branch, labels):
    """Returns list of branch names from list of labels"""
    return list(
        label2branchmap[label]
        for label in labels
        if label in label2branchmap
        and not is_same_branch(label2branchmap[label], target_branch)
    )


partial_backport_cache = {}


def get_potential_backport_ids_from_notes(mr):
    potential_backport_ids = []

    for note in mr.notes.list(iterator=True):
        if note.body.startswith("mentioned in merge request !"):
            potential_backport_ids.append(int(note.body.split("!")[1]))

    return potential_backport_ids


def find_backport_for_branch(original_mr_id, potential_backport_ids, branch, project):
    required_snippet = f"Backport of MR !{original_mr_id}"

    for mr_id in potential_backport_ids:
        mr = project.mergerequests.get(mr_id)
        if mr.target_branch == branch and required_snippet in mr.description:
            return mr

    return None


def backport_is_partial(mr, branch, project):
    """Check whether a given MR in a project leads to a partial backport,
    i.e. backport MR related to a branch has the label "Backport::Partial".
    """
    if (mr, branch) not in partial_backport_cache:
        candidates = get_potential_backport_ids_from_notes(mr)
        backport = find_backport_for_branch(mr.iid, candidates, branch, project)
        is_partial = backport is not None and "Backport::Partial" in backport.labels
        partial_backport_cache[mr, branch] = is_partial

    return partial_backport_cache[mr, branch]


def check_mr(mr, local_repo, parent_log, project) -> bool:
    """Check that backport branches in the local repo contain commit messages
    mentioning commit ID from the original branch.
    Returns True if a missing commit is detected."""
    log = ConditionalLogHeader(
        f"!{mr.iid} {mr.title} ({mr.web_url}), target branch {mr.target_branch}, "
        f"labels {mr.labels}",
        level=logging.INFO,
        parent=parent_log,
    )
    backport_branches = backport_to_branches(mr.target_branch, mr.labels)

    if any(l.startswith("Backport") for l in mr.labels):
        # sanity check, this is backport so it should not require _another_ backport
        assert not backport_branches
    if not backport_branches:
        log.info("- no backport needed, skipping this MR")
        return False
    log.info("- to be backported to: %s", backport_branches)
    problem_detected = False
    for branch in backport_branches:
        found_count = 0
        missing_count = 0
        for orig_commit in mr.commits():
            log.info("- commit %s in %s?", orig_commit.id, branch)
            found = False
            branch_tip = local_repo.branches[branch].peel(1).id
            for iter_commit in local_repo.walk(branch_tip):
                if orig_commit.short_id in iter_commit.message:
                    log.info("- ok, found in commit message for %s", iter_commit.id)
                    found = True
                    break
            if not found:
                if backport_is_partial(mr, branch, project):
                    log.info("- partial backport, not all commits must be backported")
                else:
                    log.warning(
                        "- ! commit %s not found in %s branch", orig_commit.id, branch
                    )
                    missing_count += 1
            else:
                found_count += 1
        if found_count == 0:
            log.error(
                "- !!! no trace of any of %d commits from MR !%d in %s branch",
                missing_count,
                mr.iid,
                branch,
            )
        elif missing_count > 0:
            log.warning(
                "- %d commits from MR !%d were found in %s branch, "
                "but %d other commits are missing",
                found_count,
                mr.iid,
                branch,
                missing_count,
            )
        problem_detected = bool(missing_count) or problem_detected

    return problem_detected


def filter_branches(mrs):
    """
    Yield only MRs which target bind-* branches.
    """
    for mr in mrs:
        if mr.target_branch in target_branches:
            yield mr
        else:
            logging.debug(
                f"!{mr.iid} {mr.title} ({mr.web_url}), target branch {mr.target_branch}: "
                "skipping because target branch is not a release branch"
            )


def main() -> int:
    parser = argparse.ArgumentParser(
        description="Check that all MRs were backported as they should"
    )

    parser.add_argument(
        "--repo",
        type=str,
        default=REPO_FS_PATH,
        help="local path to repository clone, must contain also _sub branches",
    )

    parser.add_argument(
        "--log",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="WARNING",
        help="log level, defaults to WARNING - only mark problematic commits, "
        "INFO prints also good commits",
    )

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log), format="%(message)s")

    local_repo = pygit2.Repository(args.repo)
    proj_pub = get_bind9_project("public")
    proj_priv = get_bind9_project("private")

    problems_found = 0
    checked = 0
    log = ConditionalLogHeader("Checking MRs in the public repo", level=logging.INFO)
    mrs = proj_pub.mergerequests.list(
        milestone="Not released yet",
        scope="all",
        state="merged",
        iterator=True,
    )
    for mr in filter_branches(mrs):
        problems_found += check_mr(mr, local_repo, log, proj_pub)
        checked += 1

    log = ConditionalLogHeader("Checking MRs in the private repo", level=logging.INFO)
    mrs = proj_priv.mergerequests.list(
        milestone="Not released yet",
        scope="all",
        state="merged",
        iterator=True,
    )
    for mr in filter_branches(mrs):
        problems_found += check_mr(mr, local_repo, log, proj_priv)
        checked += 1

    if problems_found or not checked:
        logging.critical(
            "Potential problems detected (%d suspicious MRs out of %d), "
            "manual inspection required!",
            problems_found,
            checked,
        )
    else:
        logging.info("All good, checked %d MRs", checked)
    return problems_found


if __name__ == "__main__":
    if main():
        sys.exit(5)
