#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

import argparse
import io
import logging
import os
import re

from typing import cast

import pygit2

from bindutil.gitlab import get_bind9_project
from bindutil.metadata import ReleaseMetadata
from bindutil.repository import BIND9Repository


class BIND9Version:
    def __init__(self, tree: pygit2.Tree) -> None:
        try:
            file = tree / "version"
            self.file_name = "version"
            branch_regex = rb"^MINORVER=(\d+)$"
            release_regex = rb"^PATCHVER=(\d+)$"
        except KeyError:
            file = tree / "configure.ac"
            self.file_name = "configure.ac"
            branch_regex = rb"^(m4_define\(\[bind_VERSION_MINOR\], )(\d+)(\)dnl)$"
            release_regex = rb"^(m4_define\(\[bind_VERSION_PATCH\], )(\d+)(\)dnl)$"

        self._bumped_file_lines = []

        branch = None
        release_next = None
        blob = cast(pygit2.Blob, file)
        with io.BytesIO(blob.data) as file_data:
            file_lines = file_data.readlines()
            for line in file_lines:
                match = re.match(branch_regex, line)
                if match:
                    branch = match.group(2).decode("ascii")
                match = re.match(release_regex, line)
                if match:
                    release_next = str(int(match.group(2)) + 1)
                    release_next_bytes = release_next.encode("ascii")
                    line = match.group(1) + release_next_bytes + match.group(3) + b"\n"
                self._bumped_file_lines.append(line)

        assert branch is not None and release_next is not None
        self.branch = f"9.{branch}"
        self.next = f"9.{branch}.{release_next}"

    @property
    def bump_branch(self) -> str:
        return f"set-up-version-for-bind-{self.next}"

    def write_bumped_file(self, repo: BIND9Repository) -> None:
        with repo.open_file_for_writing(self.file_name) as file:
            for line in self._bumped_file_lines:
                file.write(line)


def mode_prepare_branch(args: argparse.Namespace) -> None:
    logging.info("Preparing version bump branch for %s", args.target_branch)
    assert "/" in args.target_branch, "remote/ prefix required"

    repo = BIND9Repository(".")
    if not repo.check_status():
        return

    target_branch = repo.get_remote_branch(args.target_branch, fetch=True)
    target_branch_commit = target_branch.peel(1)
    logging.info("Target branch is at %s", target_branch_commit.id)
    version = BIND9Version(target_branch_commit.tree)
    logging.info("Next BIND version is %s", version.next)

    local_branch = f"{repo.user}/{version.bump_branch}"
    logging.info("Setting up local branch '%s'", local_branch)
    repo.checkout_branch(local_branch, target_branch)

    logging.info("Updating %s", version.file_name)
    version.write_bumped_file(repo)
    logging.info("Committing version bump")
    repo.commit_all(f"Update BIND version to {version.next}-dev")


def mode_submit_mr(args: argparse.Namespace) -> None:
    logging.info("Submitting merge request targeting branch %s", args.target_branch)

    repo = BIND9Repository(".")
    proj_pub = get_bind9_project("public")

    target_branch = repo.get_remote_branch(args.target_branch)
    target_branch_commit = target_branch.peel(1)
    logging.info("Target branch is at %s", target_branch_commit.id)
    version = BIND9Version(target_branch_commit.tree)
    logging.info("Next BIND version is %s", version.next)

    remote, target_branch_without_remote = repo.parse_branch(args.target_branch)
    local_branch = f"{repo.user}/{version.bump_branch}"
    repo.push_branch(remote, local_branch)

    metadata = ReleaseMetadata(args.metadata).data

    logging.info("Creating merge request")
    merge_request = proj_pub.mergerequests.create(
        {
            "labels": [
                "No CHANGES",
                "Release",
                f"v{version.branch}",
            ],
            "milestone_id": metadata["milestones"]["next"]["public"]["id"],
            "remove_source_branch": True,
            "source_branch": local_branch,
            "target_branch": target_branch_without_remote,
            "title": f"chg: doc: Set up version for BIND {version.next}",
        }
    )

    logging.info("Created %s", merge_request.web_url)


def main() -> None:
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)
    parser = argparse.ArgumentParser(
        description="Prepare a version bump for a given target branch",
        epilog="NOTE: this script needs to be run from a working copy of the BIND 9 Git repository",
    )
    subparsers = parser.add_subparsers(required=True)

    parser_prepare_branch = subparsers.add_parser(
        "prepare-branch",
        help="Prepare a local Git branch with the necessary commits (step 1)",
    )
    parser_prepare_branch.add_argument(
        "--target-branch",
        required=True,
        help="the branch on top of which to prepare changes; "
        "must include remote name, e.g. 'origin/main'",
    )
    parser_prepare_branch.set_defaults(func=mode_prepare_branch)

    parser_submit_mr = subparsers.add_parser(
        "submit-mr", help="Create a merge request from a local Git branch (step 2)"
    )
    parser_submit_mr.add_argument(
        "--target-branch",
        required=True,
        help="the branch to set as the target branch for the merge request; "
        "must include remote name, e.g. 'origin/main'",
    )

    default_metadata_path = os.path.join(os.path.dirname(__file__), "metadata.json")
    parser_submit_mr.add_argument(
        "--metadata",
        type=argparse.FileType(),
        default=default_metadata_path,
        help="the metadata file used for determining the milestone to assign the merge request to",
    )
    parser_submit_mr.set_defaults(func=mode_submit_mr)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
