#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Generate command skeleton for doc MR preparation
"""

import argparse
import logging
from typing import List, Union
import shlex
import sys

from bindutil import bindversion
from bindutil.metadata import ReleaseMetadata


def print_cmd(comments: Union[str, List[str]], cmds: Union[List[str], List[List[str]]]):
    if isinstance(comments, str):
        comments = [comments]
    if len(cmds) > 0 and isinstance(cmds[0], str):
        cmds = [cmds]
    for comment in comments:
        print("#", comment)
    for cmd in cmds:
        print(
            shlex.join(cmd)
            .replace("'$EDITOR'", "$EDITOR")
            .replace("'>'", ">")
            .replace("'>>'", ">>")
            .replace("'|'", "|")
        )
    print()


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Generate commands for doc MRs (based on from metadata.json)"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    parser.add_argument("version", help="e.g. 9.18.25")
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata).data
    if args.version not in metadata["versions"]:
        sys.exit(
            f"specified version not scheduled for this month: {args.version}\n"
            f"use one of: {metadata['versions']}"
        )

    version = args.version.split(".")
    prev_version_str = (
        f"{version[0]}.{version[1]}.{int(version[2])-1}" if version[2] != "0" else "XXX"
    )
    heading = f"BIND {args.version}"
    has_subscriber_version = version[1] in ["18"]

    # prep branch
    print_cmd(
        "create doc branch",
        [
            "git",
            "checkout",
            "-b",
            f"{metadata['release_engineer']}/prepare-documentation-for-bind-{args.version}",
        ],
    )

    print_cmd(
        "add COPYRIGHT to changelog",
        [
            "head",
            "-n",
            "11",
            "doc/changelog/changelog-history.rst",
            ">",
            f"doc/changelog/changelog-{args.version}.rst",
        ],
    )
    if has_subscriber_version:
        print_cmd(
            [
                "create -S changelog file (with COPYRIGHT only)",
            ],
            [
                "head",
                "-n",
                "11",
                "doc/changelog/changelog-history.rst",
                ">",
                f"doc/changelog/changelog-{args.version}-S1.rst",
            ],
        )
    print_cmd(
        "generate changelog",
        [
            [
                "./contrib/gitchangelog/gitchangelog.py",
                f"v{prev_version_str}..HEAD",
                ">>",
                f"doc/changelog/changelog-{args.version}.rst",
            ],
        ],
    )
    print_cmd(
        "update the changelog version",
        [
            [
                "sed",
                "-i",
                f"s/^(-dev)/BIND {args.version}/",
                f"doc/changelog/changelog-{args.version}.rst",
            ],
            [
                "sed",
                "-i",
                f"s/^------$/{'-' * (len(args.version) + 5)}/",
                f"doc/changelog/changelog-{args.version}.rst",
            ],
        ],
    )
    print_cmd(
        "link the changelog file(s)",
        [
            [
                "sed",
                "-i",
                f"0,/^.. include::/s//.. include:: ..\\/changelog\\/changelog-{args.version}.rst\\n&/",
                "doc/arm/changelog.rst",
            ],
        ],
    )
    if has_subscriber_version:
        print_cmd(
            "link the -S changelog file(s)",
            [
                [
                    "sed",
                    "-i",
                    f"0,/^.. include::/s//.. include:: ..\\/changelog\\/changelog-{args.version}-S1.rst\\n&/",
                    "doc/arm/changelog.rst",
                ],
            ],
        )
    print_cmd(
        "double check the changelog",
        [
            ["git", "add", "doc/"],
            ["git", "diff", "--cached"],
        ],
    )

    checks = [
        "Is the version heading correct?",
        "Is the copyright notice present?",
        "Is the changelog properly included?",
    ]
    if has_subscriber_version:
        checks += ["Is the empty -S changelog prepared and included?"]
    checks.append("commit!")

    print_cmd(
        checks,
        ["git", "commit", "-m", f"Generate changelog for BIND {args.version}"],
    )

    print_cmd(
        [
            "add COPYRIGHT to the top of relnotes",
        ],
        [
            "head",
            "-n",
            "11",
            "doc/changelog/changelog-history.rst",
            ">",
            f"doc/notes/notes-{args.version}.rst",
        ],
    )
    print_cmd(
        "generate release notes",
        [
            "GITCHANGELOG_CONFIG_FILENAME=contrib/gitchangelog/relnotes.rc.py",
            "./contrib/gitchangelog/gitchangelog.py",
            f"v{prev_version_str}..HEAD",
            ">>",
            f"doc/notes/notes-{args.version}.rst",
        ],
    )
    print_cmd(
        "update the version in release notes",
        [
            [
                "sed",
                "-i",
                f"s/^(-dev)/Notes for {heading}/",
                f"doc/notes/notes-{args.version}.rst",
            ],
            [
                "sed",
                "-i",
                f"s/^------$/{'-' * len('Notes for ' + heading)}/",
                f"doc/notes/notes-{args.version}.rst",
            ],
        ],
    )
    print_cmd(
        "link release notes to ARM",
        [
            "sed",
            "-i",
            f"0,/^\\.\\. include/{{s|\\.\\. include::|.. include:: ../notes/notes-{args.version}.rst\\n.. include::|}}",
            "doc/arm/notes.rst",
        ],
    )
    print_cmd(
        "double check what you did!",
        [
            ["git", "add", "doc/"],
            ["git", "diff", "--cached"],
        ],
    )
    print_cmd(
        [
            "Is the version heading correct?",
            "Is the copyright notice present?",
            "Are the release notes properly included?",
            "commit!",
        ],
        ["git", "commit", "-m", f"Prepare release notes for BIND {args.version}"],
    )

    print_cmd(
        "OPTIONAL add missing notes",
        [
            ["$EDITOR", f"doc/notes/notes-{args.version}.rst"],
            ["git", "commit", "-a", "-m", "Add release note for GL #"],
        ],
    )

    print_cmd(
        [
            "add rst markup to release notes",
            "tweak and reword release notes",
        ],
        [
            ["$EDITOR", f"doc/notes/notes-{args.version}.rst"],
            ["git", "commit", "-a", "-m", "Tweak and reword release notes"],
        ],
    )

    print_cmd(
        "ensure docs can build and look sane",
        [
            ["autoreconf", "-f", "-v", "-i"],
            ["./configure"],
            ["make", "doc"],
            ["xdg-open", "doc/arm/_build/html/notes.html"],
            ["xdg-open", "doc/arm/_build/html/changelog.html"],
        ],
    )

    cmd = [
        "git",
        "push",
        "private",
        "-o",
        "merge_request.create",
        "-o",
        f"merge_request.title=new: doc: Prepare documentation for BIND {args.version}",
        "-o",
        f'merge_request.milestone={metadata["milestones"]["current"]["private"]["title"]}',
        "-o",
        f"merge_request.target=v{args.version}-release",
        # "-o",
        # f"merge_request.description=",
    ]

    labels = ["No CHANGES", "Documentation", "Review"]
    ver_label = bindversion.normalize_label(args.version)
    labels += [ver_label]

    for label in labels:
        cmd += ["-o", f"merge_request.label={label}"]

    print(shlex.join(cmd))


if __name__ == "__main__":
    main()
