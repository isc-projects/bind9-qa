#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

import argparse
import collections
import logging
import gitlab
import io
import sys
import zipfile

from pathlib import Path


def job_sortkey(job):
    return (job.stage, job.name, job.id)


def get_artifacts(project, job):
    project_job = project.jobs.get(job.id, lazy=True)
    print(f"Downloading artifacts ZIP for job {job.name}...", file=sys.stderr)
    release_zip = project_job.artifacts()
    print(f"Extracting artifacts for job {job.name}...", file=sys.stderr)
    return zipfile.ZipFile(io.BytesIO(release_zip))


def process_release_job(project, job):
    archive = get_artifacts(project, job)
    archive.extractall(path=job.name)

    print()
    print("---+++ Documentation Check")
    print()
    print("   * *README*")
    print("      * icon:led-yellow WIP")
    print()
    print("   * *CHANGES*")
    print("      * icon:led-yellow WIP")
    print()
    print("   * *Release Notes (HTML)*")
    print("      * icon:led-yellow WIP")
    print()
    print("   * *Release Notes (PDF)*")
    print("      * icon:led-yellow WIP")
    print()
    print("   * *Release Notes (Text)*")
    print("      * icon:led-yellow WIP")
    print()


def process_stress_jobs(project, all_jobs):
    print()
    print("---+++ Stress Tests")
    print()
    stress_jobs = [
        project.jobs.get(j.id) for j in all_jobs if j.name.startswith("stress:")
    ]
    for job in stress_jobs:
        outpath = Path(f"{job.name}.{job.id}")
        if outpath.exists():  # TODO
            continue
        outpath.mkdir(exist_ok=True)
        with open(outpath / "job.log", "wb") as logf:
            logf.write(job.trace())
        if job.status != "success":
            continue
        artifacts = get_artifacts(project, job)
        extract = [name for name in artifacts.namelist() if name.endswith(".png")]
        artifacts.extractall(path=outpath, members=extract)


def doit(project, pipeline):
    jobs = sorted(
        pipeline.jobs.list(all=True, include_retried=True),
        key=job_sortkey,
    )

    job_counter = collections.Counter(j.name for j in jobs)
    retried_jobs = {job_name for job_name, count in job_counter.items() if count > 1}
    print(retried_jobs)

    interesting_jobs = [
        j
        for j in jobs
        if j.status not in {"success", "manual"} or j.name in retried_jobs
    ]
    for job in sorted(interesting_jobs, key=job_sortkey):
        print(job.name, job.status, job.web_url)

    # from IPython.core.debugger import set_trace
    # set_trace()

    release_jobs = [j for j in jobs if j.name == "release"]
    assert len(release_jobs) <= 1
    if release_jobs:
        process_release_job(project, release_jobs[0])

    process_stress_jobs(project, jobs)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--project-id", type=int, default=9)
    parser.add_argument("pipeline_id_or_tag", type=str)
    args = parser.parse_args()

    gitlab_obj = gitlab.Gitlab.from_config()
    project = gitlab_obj.projects.get(args.project_id, lazy=True)

    try:
        pipeline_id = int(args.pipeline_id_or_tag)
    except ValueError as exc:
        logging.info(
            "trying to find latest pipeline for ref %s", args.pipeline_id_or_tag
        )
        pipelines = project.pipelines.list(
            ref=args.pipeline_id_or_tag, order_by="id", per_page=2
        )
        if not pipelines:
            raise argparse.ArgumentTypeError(
                "no pipeline matching given ref found"
            ) from exc
        if len(pipelines) != 1:
            logging.warning(
                "more than one pipeline found, double check you are looking at the right one!"
            )
            logging.warning("pipelines: %s", pipelines)
        pipeline_id = pipelines[0].id
    pipeline = project.pipelines.get(id=pipeline_id)
    if not pipeline.tag:
        logging.warning("pipeline %d does NOT belong to A TAG", pipeline.id)

    doit(project, pipeline)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
