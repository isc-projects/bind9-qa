#!/usr/bin/env python3
"""
Assign released MRs to versioned milestones
"""

import argparse
import logging
import sys
from typing import Iterable

import gitlab
import gitlab.v4.objects

from bindutil import bindversion
from bindutil.gitlab import get_bind9_project
from bindutil.metadata import ReleaseMetadata


EOL_MILESTONES = {
    "bind-9.11": "9.11-EoL",
    "bind-9.11-sub": "9.11-S-EoL",
    "bind-9.16": "9.16-EoL",
    "bind-9.16-sub": "9.16-S-EoL",
}


def get_or_create_milestone(
    project: gitlab.v4.objects.Project, title: str
) -> gitlab.v4.objects.ProjectMilestone:
    milestones = list(project.milestones.list(title=title, get_all=True))
    if not milestones:
        logging.info(
            f'creating new milestone "{title}" in {project.path_with_namespace}'
        )
        milestone = project.milestones.create({"title": title})
    elif len(milestones) == 1:
        milestone = milestones[0]
        logging.info(
            f'opening existing milestone "{title}" in {project.path_with_namespace}'
        )
        if not milestone.state == "active":
            milestone.state_event = "activate"
            milestone.save()
    else:
        raise ValueError(
            f'multiple milestones with title "{title} in {project.path_with_namespace}"'
        )
    return milestone


def close_milestones(milestones: Iterable[gitlab.v4.objects.ProjectMilestone]):
    for milestone in milestones:
        logging.info(f'closing milestone "{milestone.title}" ({milestone.id})')
        milestone.state_event = "close"
        milestone.save()


def move_merge_requests(project: gitlab.v4.objects.Project, metadata: ReleaseMetadata):
    milestones = {}
    for version in metadata.data["versions"]:
        branch = bindversion.version2branch(version)
        release_branch = f"v{version}-release"
        milestone = get_or_create_milestone(project, version)
        milestones[branch] = milestone
        milestones[release_branch] = milestone

    for branch, title in EOL_MILESTONES.items():
        milestone = get_or_create_milestone(project, title)
        milestones[branch] = milestone

    mrs = project.mergerequests.list(
        milestone="Not released yet",
        scope="all",
        state="merged",
        get_all=True,  # don't use iterator -- milestone changes during iteration!
    )

    manual_action_required = 0
    assigned = 0
    for mr in mrs:
        try:
            milestone = milestones[mr.target_branch]
        except KeyError:
            mr_url = f"{project.web_url}/-/merge_requests/{mr.iid}"
            logging.error(
                f'unknown target branch "{mr.target_branch}", assign milestone manually: {mr_url}'
            )
            manual_action_required += 1
            continue
        logging.info(
            f'assigning {project.path_with_namespace}!{mr.iid} to milestone "{milestone.title}"'
        )
        mr.milestone_id = milestone.id
        mr.save()

        assigned += 1

    logging.info(
        f"assigned {assigned} MRs to versioned milestones in {project.path_with_namespace}"
    )

    closed_mrs = project.mergerequests.list(
        milestone="Not released yet",
        scope="all",
        state="closed",
        get_all=True,
    )

    closed = 0
    for mr in closed_mrs:  # remove milestone from closed MRs
        mr.milestone_id = 0
        mr.save()
        closed += 1

    logging.info(f"removed milestone from {closed} closed MRs")

    close_milestones(set(milestones.values()))

    if manual_action_required:
        logging.warning(
            f"{manual_action_required} MRs require manual assignment in {project.path_with_namespace}"
        )
        return False

    return True


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata)
    proj_pub = get_bind9_project("public")
    proj_priv = get_bind9_project("private")

    ok = True
    ok *= move_merge_requests(proj_pub, metadata)
    ok *= move_merge_requests(proj_priv, metadata)

    if ok:
        logging.info("ALL DONE!")
    else:
        logging.error("MANUAL ASSIGNMENT REQUIRED")
        sys.exit(1)


if __name__ == "__main__":
    main()
