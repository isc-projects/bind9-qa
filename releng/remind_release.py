#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Generate reminder about upcoming code freeze.
"""

import argparse
import datetime
import logging
import os

import gitlab

from bindutil.messages import message_to_mattermost
from bindutil.metadata import ReleaseMetadata


def main():
    gl_url = os.getenv("CI_SERVER_URL", None)
    if gl_url is not None:
        token = os.getenv("BIND_TEAM_API_TOKEN")
        gl = gitlab.Gitlab(gl_url, token)
    else:  # running locally on dev machine
        gl = gitlab.Gitlab.from_config()

    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Generate reminder message about upcoming code freeze"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    parser.add_argument(
        "--channel", default="bind9", type=str, help="mattermost channel"
    )
    parser.add_argument(
        "--message-mattermost",
        action="store_true",
        help="set to send message to mattermost",
    )
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata).data
    today = datetime.date.today()

    project = gl.projects.get("isc-projects/bind9")
    issues = project.issues.list(labels=["Release"], state="opened")
    assert issues, "No relesease checklist issue!"
    issue = issues[0]
    release_checklist_link = (
        "https://gitlab.isc.org/isc-projects/bind9/-/issues/" + str(issue.iid)
    )

    release_engineer = metadata["release_engineer"]
    messages = {
        "public": f"@here BIND9 versions 9.18.x, 9.18.x-S1, 9.20.x, 9.21.x are [scheduled]({release_checklist_link}) for release later _today_. Please contact @{release_engineer} or QA if there's any reason to withhold today's release.",
        "asn": f"@here BIND9 ASN is [scheduled]({release_checklist_link}#before-the-asn-deadline) for _today_.",
    }
    for kind, message in messages.items():
        due_date = metadata["schedule"][kind]

        if due_date != today:
            logging.info("no %s reminder today!", kind)
            continue

        if args.message_mattermost:
            message_to_mattermost(message, args.channel)
            logging.info(
                "%s reminder sent to mattermost channel: %s", kind, args.channel
            )
        else:
            logging.info("use --message-mattermost to send message to mattermost")
            print(message)


if __name__ == "__main__":
    main()
