#!/usr/bin/env python3
"""
Auto-backporter

Inputs:
- env vars: BACKPORT_GITLAB_API_TOKEN + CI_SERVER_URL
  - Token must allow pushing new branches and MR manipulation
- CLI: Gitlab Project ID + MR ID
  - reads v9.?? labels from the MR
- local clone of target branch with MR's merge commit on top

Outputs:
- New branch(es) with backports pushed into the same project
  - Branches might be empty if automatic cherry-pick fails
- New MRs created in the same project, with all the right labels etc.
  - To be reused for subsequent merge
"""

import argparse
import subprocess
import os

import logging
import re
import shlex
import sys

from bindutil.gitlab import get_isc_gitlab

from bindutil.bindversion import label2branchmap as label2branch, branch2label


def backport_to_branches(target_branch, labels):
    return list(
        label2branch[label]
        for label in labels
        if label in label2branch and label2branch[label] != target_branch
    )


def create_backport_mr(source_mr, target_branch, assignee=None):
    omit_labels = ["LGTM (Merge OK)", "Review"]
    omit_labels.extend(label2branch.keys())

    assert target_branch in branch2label

    create_params = {
        "target_branch": target_branch,
        "remove_source_branch": True,
        "allow_collaboration": True,
        "assign": assignee or (source_mr.assignee and source_mr.assignee["username"]),
    }
    if source_mr.milestone:
        create_params.update(
            {
                "milestone_id": source_mr.milestone["id"],
                "milestone_title": source_mr.milestone["title"],
            }
        )

    create_params["title"] = f"[{target_branch[5:]}] {source_mr.title}"
    create_params["labels"] = list(
        label for label in source_mr.labels if label not in omit_labels
    ) + ["Backport", branch2label[target_branch]]
    create_params["description"] = (
        f"{source_mr.description.strip()}\n\nBackport of MR !{source_mr.iid}"
    ).replace(
        "\n", r"\n"
    )  # use literal newlines (gitlab workaround for multiline options)
    return create_params


def cherrypick_cmd(source_mr):
    commits = list(source_mr.commits())
    # API returns commits in reverse order (topmost/latest goes first)
    first_commit = commits[-1]
    last_commit = commits[0]
    yield {
        "type": "cmd",
        "cmd": ["git", "cherry-pick", "-x", f"{first_commit.id}^..{last_commit.id}"],
        "continue_on_failure": True,
    }


def push_cmd(params):
    opts = []
    opts.extend(["-o", "merge_request.create"])
    opts.extend(["-o", f'merge_request.target={params["target_branch"]}'])
    opts.extend(["-o", f'merge_request.title={params["title"]}'])
    if "assign" in params:
        opts.extend(["-o", f'merge_request.assign={params["assign"]}'])
    if "milestone_title" in params:
        opts.extend(["-o", f'merge_request.milestone={params["milestone_title"]}'])
    opts.extend(["-o", f'merge_request.description={params["description"]}'])
    for label in params["labels"]:
        opts.extend(["-o", f"merge_request.label={label}"])
    return opts


def generate_cmds(source_mr, target_branch: str, target_remote: str, assignee: str):
    assert target_branch.startswith("bind-9.")
    backport_branch = f"backport-{source_mr.source_branch}-{target_branch[5:]}"
    wrkpath = f"/tmp/mr{source_mr.iid}-{target_branch}"
    yield {
        "type": "cmd",
        "cmd": ["git", "fetch", "--depth=1", target_remote, target_branch],
    }
    yield {
        "type": "cmd",
        "cmd": ["git", "worktree", "add", "-b", backport_branch, wrkpath, "FETCH_HEAD"],
    }

    yield {"type": "pushd", "dir": wrkpath}
    yield from cherrypick_cmd(source_mr)
    params = create_backport_mr(source_mr, target_branch, assignee)
    yield {
        "type": "cmd",
        "cmd": ["git", "push", "--set-upstream", target_remote, backport_branch]
        + push_cmd(params),
    }
    yield {"type": "popd"}
    yield {"type": "cmd", "cmd": ["git", "worktree", "remove", wrkpath]}


def get_shell_cmd(cmdspec):
    if cmdspec["type"] == "pushd":
        return ["pushd", cmdspec["dir"]]
    if cmdspec["type"] == "popd":
        return ["popd"]
    return cmdspec["cmd"]


def extract_mr_url(results, server_url):
    match = None
    for result in results:
        if "output" not in result:
            continue

        match = re.search(
            f"remote: *({server_url}.*/merge_requests/[0-9]+)", result["output"]
        )
        if not match:
            continue
        break

    return match and match.group(1)


def apply_cmds(cmds):
    results = []
    cwd = None
    for cmdspec in cmds:
        # we don't use actual shell, so just simulate it
        # so a mere mortal can interpret the command flow
        # (and it also avoids noise)
        if cmdspec["type"] == "pushd":
            cwd = cmdspec["dir"]
            results.append({"cmd": get_shell_cmd(cmdspec), "output": "", "ret": 0})
            continue
        if cmdspec["type"] == "popd":
            cwd = None
            results.append({"cmd": get_shell_cmd(cmdspec), "output": "", "ret": 0})
            continue

        try:
            logging.info("going to run command: %s", cmdspec)
            output = subprocess.check_output(
                cmdspec["cmd"],
                stderr=subprocess.STDOUT,
                encoding="utf-8",
                cwd=cwd,
            )
            results.append(cmdspec.copy())
            results[-1].update({"output": output, "ret": 0})
        except subprocess.CalledProcessError as ex:
            results.append(cmdspec.copy())
            results[-1].update({"output": ex.output, "ret": ex.returncode})
        except FileNotFoundError as ex:
            results.append(cmdspec.copy())
            results[-1].update({"note": f"internal error: {ex}", "ret": 999})
        logging.info("result: %s", results[-1])

        if results[-1]["ret"] != 0 and not cmdspec.get(
            "continue_on_failure"
        ):  # this is the only command which is allowed to fail
            results.append({"note": "execution stopped prematurely", "ret": 999})
            break

    trouble = any("note" in result or result.get("ret") for result in results)
    return trouble, results


def post_comment(source_mr, target_branch, results, trouble, cmds, server_url):
    new_mr_url = extract_mr_url(results, server_url)
    lines = [
        f"## {target_branch} backport",
        f"This MR is labeled for backporting into {target_branch}.",
    ]

    if trouble:
        lines += ["An automated backport has **FAILED** :boom: :"]
    else:
        lines += ["An automated backport is **ready for review**: :eyes:"]
    if new_mr_url:
        lines += ["", "Continue work in this new MR :point_right:", new_mr_url]

    for result in results:
        logging.debug("result: %s", result)
        if result["ret"]:
            lines += [":boom: :point_down:"]
        if "cmd" in result:
            lines += [
                "```",
                f"{' '.join(shlex.quote(part) for part in result['cmd'])}",
                "```",
            ]
        if "output" in result and result["output"]:
            lines += ["```", result["output"], "```"]
        if "note" in result:
            lines += [result["note"]]
        elif result["ret"]:
            lines += [str(result.get("ret"))]

    if trouble:
        lines += [
            "",
            "------",
            "",
            "Here is the script we tried for an automated backport:",
            "```",
        ]
        for cmdspec in cmds:
            lines += [" ".join(shlex.quote(part) for part in get_shell_cmd(cmdspec))]
        lines += ["```"]

    for line in lines:
        logging.info("%s", line)

    source_mr.notes.create({"body": "\n".join(lines)})
    logging.info("Posted to %s", source_mr.web_url)
    return trouble


def main():
    logging.basicConfig(level=logging.WARNING, format="%(message)s")

    parser = argparse.ArgumentParser(description="generate commands for backports")

    parser.add_argument(
        "projectid",
        type=int,
        help="Gitlab project ID to read MR from",
    )
    parser.add_argument("mrid", type=int, help="MR # to be backported")
    parser.add_argument(
        "--assignee",
        type=str,
        help="argument for git push -o mr.assign= - defauls to GITLAB_USER_LOGIN",
        default=os.environ.get("GITLAB_USER_LOGIN"),
    )
    args = parser.parse_args()
    logging.info("source MR %s", args)

    gl = get_isc_gitlab(with_write_permissions=True)
    proj = gl.projects.get(args.projectid)

    logging.info("target project: %s", proj.path_with_namespace)
    mr = proj.mergerequests.get(args.mrid)

    logging.info("MR labels: %s", mr.labels)
    if "Backport" in mr.labels:
        logging.critical("MR is backport, ignoring")
        sys.exit(0)
    if mr.state != "merged":
        raise ValueError("MR is not merged yet, merge it first")

    backport_to = backport_to_branches(mr.target_branch, mr.labels)
    logging.info("To be backported to: %s", backport_to)

    any_trouble = False
    for target_branch in sorted(backport_to, reverse=True):
        logging.info("Target branch: %s", target_branch)
        cmds = list(generate_cmds(mr, target_branch, "origin", args.assignee))
        new_trouble, results = apply_cmds(cmds)
        any_trouble = any_trouble or new_trouble
        post_comment(mr, target_branch, results, any_trouble, cmds, gl.url)

    sys.exit(any_trouble)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
