#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Generate Release Checklist issue
"""

import argparse
import logging
import urllib.parse
from typing import Dict

from bindutil import bindversion
from bindutil.gitlab import get_isc_gitlab, get_bind9_project
from bindutil.messages import template_to_message, message_to_file
from bindutil.metadata import ReleaseMetadata


class DocLinker:
    def __init__(self, proj_map, milestone_title):
        self.proj_map = proj_map
        self.milestone_title = milestone_title

    def _web_url(self, version: str, listtype: str, web_params: Dict) -> str:
        if listtype == "issues":
            proj = get_bind9_project("public")
        else:
            proj = self.proj_map.version2project(version)
        gl = get_isc_gitlab()
        proj_web_url = urllib.parse.urljoin(gl.url, proj.path_with_namespace)
        return f"{proj_web_url}/-/{listtype}?{urllib.parse.urlencode(web_params)}"

    def link_closed_issue_without_relnote(self, version):
        """Closed issues assigned to the milestone without a release note"""
        web_params = (
            ("scope", "all"),
            ("sort", "created_asc"),
            ("state", "closed"),
            ("milestone_title", self.milestone_title),
            ("not[label_name][]", "Release Notes"),
            ("not[label_name][]", "Duplicate"),
            ("label_name[]", bindversion.normalize_label(version)),
        )
        return self._web_url(version, "issues", web_params)

    def link_merged_mr_without_relnote(self, version):
        """Merge requests merged into the milestone without a release note"""
        web_params = (
            ("scope", "all"),
            ("sort", "merged_at"),
            ("state", "merged"),
            ("milestone_title", "Not released yet"),
            ("not[label_name][]", "Release Notes"),
            ("target_branch", bindversion.version2branch(version)),
        )
        return self._web_url(version, "merge_requests", web_params)

    def link_merged_mr_without_changes(self, version):
        """Merge requests merged into the milestone without a CHANGES entry"""
        web_params = (
            ("scope", "all"),
            ("sort", "merged_at"),
            ("state", "merged"),
            ("milestone_title", "Not released yet"),
            ("label_name[]", "No CHANGES"),
            ("target_branch", bindversion.version2branch(version)),
        )
        return self._web_url(version, "merge_requests", web_params)


def extract_release_checklist(text):
    lines = text.splitlines()
    i = 0
    while i < len(lines):
        if lines[i] == "## Release Checklist":
            return "\n".join(lines[i + 2 :])
        i += 1
    return ""


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Prepare release checklist based on metadata.json"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata).data

    proj_map = bindversion.GitlabProjectMapper()
    doc_linker = DocLinker(
        proj_map,
        metadata["milestones"]["current"]["public"]["title"],
    )

    versions = sorted(metadata["versions"])
    message = template_to_message(
        "release_checklist_issue.md.j2",
        {
            "schedule": metadata["schedule"],
            "milestone": metadata["milestones"]["current"]["public"]["title"],
            "asn": metadata["schedule"]["asn"] is not None,
            "closed_issues_without_relnote": dict(
                (version, doc_linker.link_closed_issue_without_relnote(version))
                for version in versions
            ),
            "merged_mrs_without_relnote": dict(
                (version, doc_linker.link_merged_mr_without_relnote(version))
                for version in versions
            ),
            "merged_mrs_without_changes": dict(
                (version, doc_linker.link_merged_mr_without_changes(version))
                for version in versions
            ),
            "release_engineer": metadata["release_engineer"],
        },
    )
    out_path = message_to_file(message, "release_checklist_issue.md")
    logging.info("Release checklist written to: %s", out_path)

    # TODO: optionally, create the gitlab issue through API


if __name__ == "__main__":
    main()
