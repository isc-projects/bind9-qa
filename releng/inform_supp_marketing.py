#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Inform support & marketing about the upcoming releases and schedule.
"""

import argparse
import logging

from bindutil.gitlab import get_bind9_project
from bindutil.messages import (
    template_to_message,
    message_to_file,
    message_to_mattermost,
)
from bindutil.metadata import ReleaseMetadata


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Generate information message for marketing and support"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    parser.add_argument(
        "--channel", default="support", type=str, help="mattermost channel"
    )
    parser.add_argument(
        "--message-mattermost",
        action="store_true",
        help="set to send message to mattermost",
    )
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata)
    proj_pub = get_bind9_project("public")

    cves = proj_pub.issues.list(
        milestone=metadata.data["milestones"]["current"]["public"]["title"],
        labels=["Security"],
    )
    # sanity checks
    checklists = proj_pub.issues.list(
        state="opened",
        milestone=metadata.data["milestones"]["current"]["public"]["title"],
        labels=["Release"],
        search=metadata.data["versions"][0],
    )
    assert (
        len(checklists) == 1
    ), f"did not find exactly one open release checklist! {checklists}"

    data = {
        "versions": sorted(metadata.data["versions"]),
        "schedule": metadata.data["schedule"],
        "checklist_url": checklists[0].web_url,
        "cves": cves,
    }

    message = template_to_message("inform_support.md.j2", data)
    out_path = message_to_file(message, "inform_support.md")
    logging.info("Generated release information message: %s", out_path)
    if args.message_mattermost:
        logging.info("sending release information message to mattermost")
        message_to_mattermost(message, args.channel)


if __name__ == "__main__":
    main()
