#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Generate message about impeding code freeze.
"""

import argparse
import datetime
import logging

from bindutil.metadata import ReleaseMetadata


def format_time_delta(delta):
    return str(delta).replace(", 0:00:00", "")


def announce_text_future(metadata, today, schedule_text):
    sched = metadata["schedule"]
    text = f"""@here This is a reminder that the code freeze for {sched['freeze'].strftime('%B')} releases is **in {format_time_delta(sched['freeze'] - today)}** from today (on {sched['freeze'].strftime('%A, %B %d')}).  Please keep that in mind when prioritizing your work. This time releases are planned for BIND """
    text += ", ".join(sorted(metadata["versions"]))
    text += ".\n\n"

    text += f"""The current release schedule is as follows:

{schedule_text}"""

    text += f"""
_If you can already tell that some of the GitLab issues/MRs assigned to you won't make the cut, please move them to the {metadata['milestones']['next']['public']['title']} milestone, thanks._"""
    return text


def announce_text_now(metadata):
    sched = metadata["schedule"]
    text = f"""@here: the code freeze for {sched['freeze'].strftime('%B')} is now in effect. Please refrain from merging changes which would affect BIND **{', '.join(sorted(metadata['versions']))}** until further notice (which should happen early next week if all goes well).

If you have any documentation-only fixes/improvements queued, those are a welcome exception. If some other really urgent code/test change needs merging in your opinion, please contact QA and/or @ondrej before proceeding. The rest will be deferred to the next cycle. Thank you for your cooperation.
"""
    return text


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Generate information message for marketing and support"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    args = parser.parse_args()

    metadata = ReleaseMetadata(args.metadata)
    schedule_text = metadata.schedule_text()

    today = datetime.date.today()
    if (metadata.data["schedule"]["freeze"] - today).days > 0:
        print(announce_text_future(metadata.data, today, schedule_text))
    else:
        print(announce_text_now(metadata.data))


if __name__ == "__main__":
    main()
