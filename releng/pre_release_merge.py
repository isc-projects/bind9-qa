#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

from typing import Dict, List, Optional, Tuple, cast

import logging
import os
import re
import sys

import gitlab
import gitlab.base

from bindutil.environment import get_required_environment_variables
from bindutil.gitlab import get_bind9_project
from bindutil.messages import message_to_mattermost, template_to_message
from bindutil.repository import BIND9Repository


def filter_merge_requests_to_test(
    proj: gitlab.base.RESTObject, env: Dict[str, str]
) -> List[int]:
    merge_request_list = []
    label = env["label_filter"]
    target_branches = env["target_branches_filter"]
    target_branch_regexes = compile_target_branch_regexes(target_branches)
    logging.info(
        "🔎 Looking for merge requests to test (label: %s, target branches: %s)",
        label,
        target_branches,
    )
    merge_requests = proj.mergerequests.list(labels=label, sort="asc", iterator=True)
    for merge_request in merge_requests:
        if merge_request_is_stale(merge_request, label):
            continue
        logging.info("🧪 Starting evaluation for merge request !%d", merge_request.iid)
        if evaluate_merge_request(proj, merge_request, target_branch_regexes):
            logging.info("✅ Merge request !%d matched filter", merge_request.iid)
            merge_request_list.append(merge_request.iid)
        else:
            logging.info("❌ Merge request !%d did not match filter", merge_request.iid)
    logging.info("⭐ %d merge request(s) matched", len(merge_request_list))

    return merge_request_list


def compile_target_branch_regexes(target_branches: str) -> List[re.Pattern]:
    target_branches_list = target_branches.replace(" ", "").split(",")
    target_branch_regexes = []
    for regex in target_branches_list:
        try:
            pattern = re.compile(regex)
        except re.error:  # re.PatternError once only Python 3.13+ is supported
            logging.error("💥 Invalid branch filter regex '%s', aborting", regex)
            sys.exit(1)
        target_branch_regexes.append(pattern)
    return target_branch_regexes


def merge_request_is_stale(
    merge_request: gitlab.base.RESTObject, filter_label: str
) -> bool:
    if merge_request.state == "opened":
        return False

    logging.info(
        "🗑️ Merge request !%d is %s, removing label",
        merge_request.iid,
        merge_request.state,
    )
    merge_request.labels.remove(filter_label)
    merge_request.save()
    return True


def evaluate_merge_request(
    proj: gitlab.base.RESTObject,
    merge_request: gitlab.base.RESTObject,
    target_branch_regexes: List[re.Pattern],
) -> bool:
    logging.info("\tEvaluating merge request !%d", merge_request.iid)

    target_branch = merge_request.target_branch
    for target_branch_regex in target_branch_regexes:
        if target_branch_regex.match(target_branch):
            logging.info("\tTarget branch matches (%s)", target_branch)
            return True

    parent_merge_request = get_merge_request_for_branch(proj, target_branch)
    if not parent_merge_request:
        logging.info("\tTarget branch does not match (%s)", target_branch)
        return False

    logging.info("\tMoving to parent merge request !%d", parent_merge_request.iid)
    return evaluate_merge_request(proj, parent_merge_request, target_branch_regexes)


def get_merge_request_for_branch(
    proj: gitlab.base.RESTObject,
    target_branch: str,
) -> Optional[gitlab.base.RESTObject]:
    merge_requests = proj.mergerequests.list(
        source_branch=target_branch, page=1, per_page=2
    )
    merge_requests_list = cast(List[gitlab.base.RESTObject], merge_requests)
    assert len(merge_requests_list) <= 1
    return merge_requests_list[0] if merge_requests_list else None


def get_repo_urls(proj: gitlab.base.RESTObject, token: str) -> Tuple[str, str]:
    url = proj.http_url_to_repo
    url_with_credentials = url.replace("https://", f"https://token:{token}@")
    return url, url_with_credentials


def create_stub_repository(url: str, url_with_credentials: str) -> BIND9Repository:
    logging.info("📁 Creating stub Git repository for %s", url)
    fetch_depth = int(os.environ.get("GIT_DEPTH", "0"))
    return BIND9Repository.stub(url_with_credentials, "git", fetch_depth=fetch_depth)


def prepare_local_branch(repo: BIND9Repository, fork_from: str) -> str:
    local_branch_name = f"{fork_from}-PRE-RELEASE"
    remote_branch = repo.get_remote_branch(f"origin/{fork_from}", fetch=True)
    logging.info("🌿 Checking out new local branch %s", local_branch_name)
    repo.checkout_branch(local_branch_name, remote_branch)
    return local_branch_name


def process_merge_requests(
    proj: gitlab.base.RESTObject,
    env: Dict[str, str],
    repo: BIND9Repository,
    iids: List[int],
) -> bool:
    successful_merges = 0

    for iid in iids:
        logging.info("⤵️ Retrieving merge request !%d", iid)
        merge_request = proj.mergerequests.get(iid)

        suffix = os.environ.get(
            "REPLACEMENT_BRANCH_SUFFIX", env["branch_to_merge_into"]
        )
        branch_to_merge, commit_message = get_branch_to_merge(
            repo, merge_request, suffix
        )

        try:
            merge_branch(repo, branch_to_merge, commit_message)
        except BIND9Repository.UnresolvedMergeConflicts:
            logging.error("💥 Unresolved merge conflicts found, aborting")
            post_mattermost_failure_notification(env, merge_request, successful_merges)
            return False

        successful_merges += 1

    return True


def get_branch_to_merge(
    repo: BIND9Repository,
    merge_request: gitlab.base.RESTObject,
    replacement_branch_suffix: str,
) -> Tuple[str, str]:
    default_branch = merge_request.source_branch
    replacement_branch = look_for_replacement_branch(
        repo, default_branch, replacement_branch_suffix
    )

    if replacement_branch:
        logging.info(
            "🔄 Merging branch '%s' instead of merge request !%d",
            replacement_branch,
            merge_request.iid,
        )
        commit_message = f"Merge MR{merge_request.iid} (branch '{replacement_branch}')"
        return replacement_branch, commit_message

    logging.info("\tUsing merge request branch '%s'", default_branch)
    commit_message = f"Merge MR{merge_request.iid}"
    return default_branch, commit_message


def look_for_replacement_branch(
    repo: BIND9Repository, default_branch: str, suffix: str
) -> Optional[str]:
    branch_name_base = get_branch_name_without_version_suffix(default_branch)
    replacement_branch = f"{branch_name_base}-{suffix}"
    if replacement_branch == default_branch:
        return None

    logging.info("\tChecking whether branch '%s' exists", replacement_branch)
    replacement_exists = repo.remote_branch_exists(f"origin/{replacement_branch}")

    return replacement_branch if replacement_exists else None


def get_branch_name_without_version_suffix(branch_name: str) -> str:
    versioned_branch_name_regex = r"^(?P<branch_name_base>.*?)-(bind-)?9\.[0-9]+$"
    match = re.match(versioned_branch_name_regex, branch_name)
    return match.group("branch_name_base") if match else branch_name


def merge_branch(repo: BIND9Repository, branch_name: str, commit_message: str) -> None:
    branch = repo.get_remote_branch(f"origin/{branch_name}", fetch=True)
    branch_tip = branch.peel(1)

    with repo.merge(branch_tip, commit_message) as conflicts:
        for path in conflicts:
            if path.startswith("CHANGES") or path.startswith("doc/notes/"):
                logging.info("✨ Resolving merge conflict for %s", path)
                _, file_contents_in_merge_request = conflicts[path]
                with repo.open_file_for_writing(path) as file:
                    file.write(file_contents_in_merge_request)
                repo.add_to_index(path)


def post_mattermost_failure_notification(
    env: Dict[str, str], merge_request: gitlab.base.RESTObject, successful_merges: int
) -> None:
    logging.info("💬 Posting Mattermost notification to %s", env["mattermost_channel"])

    mentions = []
    if merge_request.author:
        mentions.append(merge_request.author["username"])
    if merge_request.assignee:
        mentions.append(merge_request.assignee["username"])
    for assignee in merge_request.assignees:
        mentions.append(assignee["username"])
    for reviewer in merge_request.reviewers:
        mentions.append(reviewer["username"])

    message = template_to_message(
        "pre_release_merge_failed.md.j2",
        {
            "job_url": env["ci_job_url"],
            "mentions": sorted(set(mentions)),
            "merge_request": merge_request,
            "successful_merges": successful_merges,
            "target_branch": env["branch_to_merge_into"],
        },
    )
    message_to_mattermost(message, env["mattermost_channel"])


def push_local_branch(repo: BIND9Repository, branch_name: str) -> None:
    repo.push_branch("origin", branch_name, force=True)
    logging.info("⬆️ Pushed %s to %s", repo.head, branch_name)


def trigger_pipeline(
    proj: gitlab.base.RESTObject, env: Dict[str, str], branch_name: str
) -> None:
    token = env["ci_job_token"]
    variables = get_downstream_pipeline_variables()
    pipeline = proj.trigger_pipeline(branch_name, token, variables)
    logging.info("🔗 Created %s", pipeline.web_url)


def get_downstream_pipeline_variables() -> Dict[str, str]:
    downstream_variables = {}
    downstream_prefix = "DOWNSTREAM_"
    for variable, value in os.environ.items():
        if variable.startswith(downstream_prefix):
            downstream_variable = variable[len(downstream_prefix) :]
            downstream_variables[downstream_variable] = value
    return downstream_variables


def main() -> None:
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    env = get_required_environment_variables(
        [
            "bind_team_write_token",
            "branch_to_merge_into",
            "ci_job_token",
            "ci_job_url",
            "label_filter",
            "mattermost_channel",
            "mattermost_webhook_url",
            "target_branches_filter",
        ]
    )
    if not env:
        sys.exit(1)

    proj_priv = get_bind9_project("private", with_write_permissions=True)
    merge_requests = filter_merge_requests_to_test(proj_priv, env)
    always_trigger = env["branch_to_merge_into"].endswith("-release")
    if not merge_requests and not always_trigger:
        sys.exit(0)

    repo_url, repo_url_with_credentials = get_repo_urls(
        proj_priv, env["bind_team_write_token"]
    )
    repo = create_stub_repository(repo_url, repo_url_with_credentials)

    local_branch_name = prepare_local_branch(repo, env["branch_to_merge_into"])
    if not process_merge_requests(proj_priv, env, repo, merge_requests):
        sys.exit(1)
    push_local_branch(repo, local_branch_name)
    trigger_pipeline(proj_priv, env, local_branch_name)


if __name__ == "__main__":
    main()
