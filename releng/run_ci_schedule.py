#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Execute scheduled task(s) / script(s). For use in CI scheduled pipelines.
"""

import argparse
import datetime
import logging
import os
import subprocess
import sys
from zoneinfo import ZoneInfo

from bindutil.metadata import ReleaseMetadata


timezone_to_tzinfo = {
    "europe": ZoneInfo("Europe/Prague"),
    "us": ZoneInfo("America/Los_Angeles"),
    "australia": ZoneInfo("Australia/Sydney"),
}


TODAY = datetime.datetime.now().date()


def schedule_code_freeze_reminder(metadata: ReleaseMetadata):
    commands = []
    to_code_freeze = metadata.data["schedule"]["freeze"] - TODAY

    if to_code_freeze.days == 0:  # code freeze day
        logging.debug("no code freeze reminder scheduled on code freeze day")
    elif to_code_freeze.days % 7 == 0:  # every Wednesday except code freeze day
        commands.append(
            [
                "./remind_code_freeze.py",
                "--message-mattermost",
            ]
        )
        logging.info(
            "code freeze reminder scheduled (%d days to code freeze)",
            to_code_freeze.days,
        )
    else:
        logging.debug("no code freeze reminder scheduled")

    return commands


def schedule_release_reminder(metadata: ReleaseMetadata):
    commands = []
    to_release = metadata.data["schedule"]["public"] - TODAY

    if to_release.days == 0:  # release day
        commands.append(
            [
                "./remind_release.py",
                "--message-mattermost",
            ]
        )
        logging.info("public release reminder scheduled")
    else:
        logging.debug(
            "no public release reminder scheduled (%d days to code freeze)",
            to_release.days,
        )

    return commands


def main():
    global TODAY  # pylint: disable=global-statement

    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.DEBUG)

    if os.getenv("CI_SERVER_URL", None) is None:
        logging.fatal("script is meant to be executed in CI")
        sys.exit(1)

    parser = argparse.ArgumentParser(description="Run scheduled tasks")
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    parser.add_argument(
        "--timezone",
        choices=["us", "europe", "australia"],
        required=True,
        help="run taks for the selected timezone",
    )
    args = parser.parse_args()

    commands = []
    metadata = ReleaseMetadata(args.metadata)

    # make date timezone-aware
    tzinfo = timezone_to_tzinfo[args.timezone]
    TODAY = datetime.datetime.now(tzinfo).date()

    # regular tasks in europe timezone
    if args.timezone == "europe":
        commands.extend(schedule_code_freeze_reminder(metadata))
        commands.extend(schedule_release_reminder(metadata))

    failed = False
    for command in commands:
        try:
            subprocess.check_call(command)
        except subprocess.CalledProcessError as exc:
            failed = True
            logging.error("failed to execute command: %s", exc)
    if failed:
        sys.exit(1)


if __name__ == "__main__":
    main()
