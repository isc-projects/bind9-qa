#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Script to transform Gitlab milestone metadata (title and due date) into
an estimate of version numbers to be released and release schedule.
"""
import argparse
import datetime
import json
import logging
import re
import sys
from typing import Dict, Optional

import gitlab
import gitlab.v4.objects

from bindutil import bindversion
from bindutil.constants import DEVELOPERS
from bindutil.gitlab import get_bind9_project

MILESTONE_MONTH_FORMAT = "%B %Y"


class ScheduleEstimator:
    """
    Expand freeze date to rest of customary schedule.
    Custom as of 2021-12-07, must be reviewed by hand!
    """

    @staticmethod
    def next_weekday(reference: datetime.date, weekday: int) -> datetime.date:
        """weekday: 0 = Monday, 1=Tuesday, 2=Wednesday, etc."""
        assert reference.weekday() != weekday, "use + timedelta(days=7) for next week"
        assert 0 <= weekday <= 6, "Monday is 0, Sunday 6"
        return reference + datetime.timedelta(days=(weekday - reference.weekday()) % 7)

    def __init__(self, freeze_iso: str, request_asn):
        self.freeze = datetime.date.fromisoformat(freeze_iso)
        self.tag = self.next_weekday(self.freeze, 0)  # next Monday
        self.public = self.next_weekday(
            self.tag + datetime.timedelta(days=7), 2
        )  # 2nd Wednesday
        self.asn = None
        if request_asn:
            # week before public release
            self.asn = self.public - datetime.timedelta(days=7)

        assert self.freeze < self.tag < self.public
        if self.asn:
            assert self.asn < self.public

    @property
    def iso_dict(self) -> Dict[str, Optional[str]]:
        """dates as strings in ISO format or None if not appliable"""
        dates = {  # sorted chronologically
            "freeze": self.freeze.isoformat(),
            "tag": self.tag.isoformat(),
            "asn": None,
            "public": self.public.isoformat(),
        }  # type: Dict[str, Optional[str]]
        if self.asn:
            dates["asn"] = self.asn.isoformat()
        return dates


def find_exactly_one_milestone(
    project: gitlab.v4.objects.Project, substr: str
) -> gitlab.v4.objects.ProjectMilestone:
    """Gitlab API search for milestone with substring in title or description."""
    milestones = list(project.milestones.list(state="active", search=substr))
    if len(milestones) != 1:
        raise ValueError(
            "did not get exactly one milestone in {} while "
            "searching for {}, got: {}".format(
                project.path_with_namespace, substr, list(ml.title for ml in milestones)
            )
        )
    return milestones[0]


def milestone_to_versions(milestone_title: str):
    """Returns: ['9.16.24', '9.16.24-S1', '9.17.21']"""
    versions = list(
        match[0] for match in re.findall(bindversion.regex, milestone_title)
    )
    versions.sort(reverse=True)
    return versions


def milestone_to_month_year(
    milestone: gitlab.v4.objects.ProjectMilestone,
) -> datetime.date:
    """returns first day of month for a given milestone"""
    regex = "^([A-Za-z]+ 20[12][0-9]) "
    match = re.match(regex, milestone.title)
    if not match:
        raise ValueError(
            "milestone title {} does not match regex {}".format(milestone.title, regex)
        )
    from_title = datetime.datetime.strptime(
        match.group(1), MILESTONE_MONTH_FORMAT
    ).date()
    from_due = datetime.date.fromisoformat(milestone.due_date)
    if from_title != from_due.replace(day=1):
        fatal(
            "month in milestone title and due date does not match: %s != %s",
            milestone.title,
            milestone.due_date,
        )
    return from_title


def get_next_month(date: datetime.date) -> datetime.date:
    """add one month to date object; input must be on 1st day of month"""
    assert (
        date.day == 1
    ), "this hack works only for first day of month in datetime.date object"
    # guaranteed to overflow to the next month
    next_date = date + datetime.timedelta(days=32)
    return next_date.replace(day=1)


def summarize(
    public_milestone_now,
    private_milestone_now,
    schedule,
    public_milestone_next,
    private_milestone_next,
    release_engineer,
) -> Dict:
    """generate dict suitable for JSON serialization"""
    return {
        "versions": milestone_to_versions(public_milestone_now.title),
        "milestones": {
            "current": {
                "private": {
                    "title": private_milestone_now.title,
                    "id": private_milestone_now.id,
                },
                "public": {
                    "title": public_milestone_now.title,
                    "id": public_milestone_now.id,
                },
            },
            "next": {
                "private": {
                    "title": private_milestone_next.title,
                    "id": private_milestone_next.id,
                },
                "public": {
                    "title": public_milestone_next.title,
                    "id": public_milestone_next.id,
                },
            },
        },
        "schedule": schedule.iso_dict,
        "release_engineer": release_engineer,
    }


def fatal(*logmsg):
    logging.fatal(*logmsg)
    sys.exit(1)


def get_args():
    parser = argparse.ArgumentParser(
        description="Prepare metadata.json from a public milestone"
    )

    parser.add_argument(
        "milestone_substr",
        help="substring to identify currently open milestone - e.g. December",
    )
    parser.add_argument(
        "--asn",
        required=True,
        type=bool,
        help="Are we doing Advanced Security Notification?",
    )
    parser.add_argument(
        "--allow-cves-without-asn",
        action="store_true",
        help="Allow Security issue(s) in Milestone without issuing ASN",
    )
    parser.add_argument(
        "--release-engineer",
        required=True,
        type=str,
        help="mattermost username of the QA person taking care of this release",
    )
    return parser.parse_args()


def check_due_date(public, private):
    if public.due_date != private.due_date:
        fatal(
            "public milestone '%s' has mismatching due date from the private milestone '%s'",
            public.title,
            private.title,
        )


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)
    args = get_args()

    if args.release_engineer not in DEVELOPERS.keys():
        fatal(f"unknown release engineer: {args.release_engineer}")

    proj_pub = get_bind9_project("public")
    proj_priv = get_bind9_project("private")

    logging.info(
        "looking for active public milestone with substring %s", args.milestone_substr
    )
    public_milestone_now = find_exactly_one_milestone(proj_pub, args.milestone_substr)

    logging.info("extracting version numbers from public milestone name")
    versions = milestone_to_versions(public_milestone_now.title)

    logging.info("looking for -S version number")
    private_milestone_now = None
    for version in versions:
        if bindversion.is_private(version):
            logging.info("looking for -S private milestone")
            private_milestone_now = find_exactly_one_milestone(proj_priv, version)
            break
    if not private_milestone_now:
        fatal("no -S release milestone detected?!")
    if milestone_to_month_year(public_milestone_now) != milestone_to_month_year(
        private_milestone_now
    ):
        fatal(
            "public and private milestones do not refer to the same month?! %s != %s",
            public_milestone_now.title,
            private_milestone_now.title,
        )
    check_due_date(public_milestone_now, private_milestone_now)

    cves = proj_pub.issues.list(
        milestone=public_milestone_now.title, labels=["Security"]
    )
    cves += proj_priv.issues.list(
        milestone=private_milestone_now.title, labels=["Security"]
    )

    if bool(args.asn) != (len(cves) != 0):
        if args.allow_cves_without_asn:
            logging.warning(
                "The following Security issues are part of this non-ASN release: %s",
                ", ".join(f"#{cve.id} - {cve.title}" for cve in cves),
            )
        else:
            logging.fatal(
                f"ASN flag {bool(args.asn)} but {len(cves)} issues labeled "
                "Security found in current milestones"
            )
            for cve in cves:
                logging.info("%s %s", cve.web_url, cve.title)
            raise ValueError

    logging.info("looking for next month milestones for unfinished stuff")
    current_month = milestone_to_month_year(public_milestone_now)
    next_month_text = get_next_month(current_month).strftime(MILESTONE_MONTH_FORMAT)
    public_milestone_next = find_exactly_one_milestone(proj_pub, next_month_text)
    private_milestone_next = find_exactly_one_milestone(proj_priv, next_month_text)
    check_due_date(public_milestone_next, private_milestone_next)

    sched = ScheduleEstimator(public_milestone_now.due_date, args.asn)
    metadata = summarize(
        public_milestone_now,
        private_milestone_now,
        sched,
        public_milestone_next,
        private_milestone_next,
        args.release_engineer,
    )

    logging.info("going to write metadata.json, PLEASE REVIEW")
    print(json.dumps(metadata, indent=4))
    with open("metadata.json", "w", encoding="utf-8") as jsonf:
        json.dump(metadata, jsonf, indent=4)
    logging.warning("please review metadata.json")


if __name__ == "__main__":
    try:
        main()
    except ValueError as ex:
        fatal(str(ex))
