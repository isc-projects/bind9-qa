#!/usr/bin/env python3

import argparse
import logging
import os
import time

import gitlab
import pygit2

from bindutil.gitlab import get_bind9_project
from bindutil.metadata import ReleaseMetadata
from bindutil.repository import BIND9Repository


class BIND9ReleaseMerger:
    def __init__(
        self,
        repo: BIND9Repository,
        branch_commit: pygit2.Commit,
    ) -> None:
        self._repo = repo
        self._branch_commit = branch_commit

    def merge_version(self) -> None:
        self._repo.checkout_file("configure.ac", self._branch_commit)
        self._repo.checkout_file("version", self._branch_commit, must_exist=False)


def mode_prepare(args: argparse.Namespace) -> None:
    logging.info("Preparing to merge tag %s into %s", args.tag, args.target_branch)

    repo = BIND9Repository(".")
    if not repo.check_status():
        return

    target_branch = repo.get_remote_branch(args.target_branch, fetch=True)
    target_branch_commit = target_branch.peel(1)
    logging.info("Target branch is at %s", target_branch_commit.id)

    remote, target_branch_without_remote = repo.parse_branch(args.target_branch)
    local_branch = f"{target_branch_without_remote}-merge-{args.tag}"
    logging.info("Setting up local branch '%s'", local_branch)
    repo.checkout_branch(local_branch, target_branch)

    tag = repo.get_tag(args.tag, remote)
    tag_commit = tag.peel(1)
    logging.info("Tag %s is at %s", args.tag, tag_commit.id)

    commit_message = f"Merge tag '{args.tag}'"
    if target_branch_without_remote != "main":
        commit_message += f" into {target_branch_without_remote}"

    with repo.merge(tag_commit, commit_message):
        merger = BIND9ReleaseMerger(repo, target_branch_commit)
        logging.info("Fixing up BIND 9 version")
        merger.merge_version()


def mode_submit_and_merge(args: argparse.Namespace) -> None:
    logging.info("Merging tag %s into %s", args.tag, args.target_branch)

    repo = BIND9Repository(".")

    remote, target_branch_without_remote = repo.parse_branch(args.target_branch)
    local_branch = f"{target_branch_without_remote}-merge-{args.tag}"
    remote_branch = f"{args.tag}-release"
    repo.push_branch(remote, f"{local_branch}^2", push_to=remote_branch)

    metadata = ReleaseMetadata(args.metadata).data
    branch = ".".join(args.tag.split(".")[:-1])
    proj_pub = get_bind9_project("public")

    merge_request = proj_pub.mergerequests.create(
        {
            "labels": [
                "No CHANGES",
                "Release",
                branch,
            ],
            "milestone_id": metadata["milestones"]["next"]["public"]["id"],
            "source_branch": remote_branch,
            "target_branch": target_branch_without_remote,
            "title": f"chg: Merge {args.tag[1:]} release branch",
        }
    )
    logging.info("Created %s", merge_request.web_url)

    logging.info(
        "Temporarily removing branch protection for '%s'", target_branch_without_remote
    )
    proj_pub.protectedbranches.delete(target_branch_without_remote)
    repo.push_branch(remote, local_branch, push_to=target_branch_without_remote)
    logging.info("Restoring branch protection for '%s'", target_branch_without_remote)
    proj_pub.protectedbranches.create(
        {
            "name": target_branch_without_remote,
            "merge_access_level": gitlab.const.AccessLevel.DEVELOPER,
            "push_access_level": gitlab.const.AccessLevel.NO_ACCESS,
        }
    )
    repo.push_branch(remote, None, push_to=remote_branch)

    logging.info("Canceling merge request pipelines")
    for pipeline in merge_request.pipelines.list():
        project_pipeline = proj_pub.pipelines.get(pipeline.id)
        pipeline_canceled = False
        retries_left = 10
        while not pipeline_canceled and retries_left > 0:
            try:
                project_pipeline.cancel()
                logging.info("Successfully canceled pipeline %d", pipeline.id)
                pipeline_canceled = True
            except gitlab.exceptions.GitlabPipelineCancelError as exc:
                logging.warning("Failed to cancel pipeline %d: %s", pipeline.id, exc)
                logging.info("Retrying in 5 seconds (retries left: %d)", retries_left)
                retries_left -= 1
                time.sleep(5)
        if not pipeline_canceled:
            logging.error("Failed to cancel pipeline %d", pipeline.id)


def main() -> None:
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)
    parser = argparse.ArgumentParser(
        description="Merge a release tag back to a maintenance branch",
        epilog="NOTE: this script needs to be run from a working copy of the BIND 9 Git repository",
    )
    subparsers = parser.add_subparsers(required=True)

    parser_prepare = subparsers.add_parser(
        "prepare",
        help="Prepare a local Git branch with the tag merged into the target branch (step 1)",
    )
    parser_prepare.add_argument("--tag", required=True, help="the tag to merge")
    parser_prepare.add_argument(
        "--target-branch",
        required=True,
        help="the branch on top of which to prepare changes; "
        "must include remote name, e.g. 'origin/main'",
    )
    parser_prepare.set_defaults(func=mode_prepare)

    parser_submit_and_merge = subparsers.add_parser(
        "submit-and-merge",
        help="Submit and merge a merge request containing the prepared changes (step 2)",
    )
    parser_submit_and_merge.add_argument(
        "--tag", required=True, help="the tag to merge"
    )
    parser_submit_and_merge.add_argument(
        "--target-branch",
        required=True,
        help="the branch on top of which changes were previously prepared; "
        "must include remote name, e.g. 'origin/main'",
    )
    default_metadata_path = os.path.join(os.path.dirname(__file__), "metadata.json")
    parser_submit_and_merge.add_argument(
        "--metadata",
        type=argparse.FileType(),
        default=default_metadata_path,
        help="the metadata file used for determining the milestone to assign the merge request to",
    )
    parser_submit_and_merge.set_defaults(func=mode_submit_and_merge)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
