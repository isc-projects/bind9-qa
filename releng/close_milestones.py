#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Close current milestones (based on metadata.json).
Sanity checks that there are no open issues and merge requests.
"""

import json
import argparse
import logging

from bindutil.gitlab import get_bind9_project


def main():
    logging.basicConfig(format="%(levelname)8s  %(message)s", level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Close current milestones without clicking"
    )
    parser.add_argument("--metadata", type=argparse.FileType(), default="metadata.json")
    args = parser.parse_args()

    metadata = json.load(args.metadata)
    proj_pub = get_bind9_project("public")
    proj_priv = get_bind9_project("private")

    logging.info(
        "check that no open issues and merge request exist in to-be-closed milestones"
    )

    # sanity checks
    assert (
        len(
            proj_pub.issues.list(
                state="opened",
                milestone=metadata["milestones"]["current"]["public"]["title"],
                per_page=1,
            )
        )
        == 0
    )
    assert (
        len(
            proj_pub.mergerequests.list(
                state="opened",
                milestone=metadata["milestones"]["current"]["public"]["title"],
                per_page=1,
            )
        )
        == 0
    )

    assert (
        len(
            proj_priv.issues.list(
                state="opened",
                milestone=metadata["milestones"]["current"]["private"]["title"],
                per_page=1,
            )
        )
        == 0
    )
    assert (
        len(
            proj_priv.mergerequests.list(
                state="opened",
                milestone=metadata["milestones"]["current"]["private"]["title"],
                per_page=1,
            )
        )
        == 0
    )

    # close it, finally!
    logging.info("closing milestones")

    for proj, milestone_id in [
        (proj_pub, metadata["milestones"]["current"]["public"]["id"]),
        (proj_priv, metadata["milestones"]["current"]["private"]["id"]),
    ]:
        milestone = proj.milestones.get(milestone_id)
        logging.debug("%d %s", milestone_id, milestone)
        milestone.state_event = "close"
        milestone.save()

    logging.info("milestones closed")


if __name__ == "__main__":
    main()
