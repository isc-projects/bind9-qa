import logging
import os
from typing import Dict, List


def get_required_environment_variables(keys: List[str]) -> Dict[str, str]:
    env = {}

    logging.info("⚙️ Processing environment variables")
    for var in keys:
        uppercase_var = var.upper()
        value = os.environ.get(uppercase_var)
        if not value:
            logging.error("💥 Required environment variable %s not set", uppercase_var)
            return {}
        env[var] = value

    return env
