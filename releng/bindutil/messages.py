import os

from jinja2 import Environment, FileSystemLoader
import requests

from .constants import TEMPLATES_DIR, MESSAGES_DIR

JINJA_ENV = Environment(loader=FileSystemLoader(TEMPLATES_DIR))


def template_to_message(template_name, data) -> str:
    template = JINJA_ENV.get_template(template_name)
    return template.render(data)


def message_to_file(message, out_name):
    outfile = MESSAGES_DIR / out_name
    with open(outfile, "w", encoding="utf-8") as f:
        f.write(message)
    return outfile


def message_to_mattermost(message, channel=None):
    webhook_url = os.getenv("MATTERMOST_WEBHOOK_URL")

    payload = {
        "text": message,
    }
    if channel is not None:
        payload["channel"] = channel
    response = requests.post(webhook_url, json=payload, timeout=60)
    response.raise_for_status()
