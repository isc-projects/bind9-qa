"""
Utilities for handling BIND 9 version numbers, mapping to branches etc.
"""

import re

from .gitlab import get_bind9_project

label2branchmap = {
    "v9.21": "main",
    "v9.20": "bind-9.20",
    "v9.20-S": "bind-9.20-sub",
    "v9.19": None,
    "v9.18": "bind-9.18",
    "v9.18-S": "bind-9.18-sub",
    "v9.17": None,
    "v9.16": "bind-9.16",
    "v9.16-S": "bind-9.16-sub",
    "v9.11": "bind-9.11",
    "v9.11-S": "bind-9.11-sub",
}
branch2label = {branch: label for label, branch in label2branchmap.items() if branch}
target_branches = set(branch2label.keys())

compat_branch2branch = {
    "v9_18": "bind-9.18",
    "v9_18_sub": "bind-9.18-sub",
    "v9_16": "bind-9.16",
    "v9_16_sub": "bind-9.16-sub",
}

regex = "v?(?P<version>(?P<major>9\\.[0-9]+)(?P<minor>\\.[0-9]+)?((?P<sub>-S)[0-9]*)?)"


def normalize_label(label):
    """
    Normalize label to "v<major>.<minor>-S format"
    >>> normalize_label('v9.11.33-S99')
    'v9.11-S'
    >>> normalized_label('9.16.3')
    'v9.16'
    >>> normalize_label('9.17')
    'v9.17'
    """
    m = re.match(regex, label)
    if not m:
        raise ValueError(f"label {label} did not match regex {regex}")
    majorv = m.group("major")
    sub = m.group("sub") or ""
    normalized_label = f"v{majorv}{sub}"
    return normalized_label


def version2branch(version):
    return label2branchmap[normalize_label(version)]


def is_same_branch(b1, b2):
    if b1 == b2:
        return True
    b1 = compat_branch2branch.get(b1, b1)
    b2 = compat_branch2branch.get(b2, b2)
    return b1 == b2


def is_private(version):
    normalized = normalize_label(version)
    return normalized.endswith("-S")


class GitlabProjectMapper:
    def version2project(self, version: str):
        mnemonic = "private" if is_private(version) else "public"
        return get_bind9_project(mnemonic)


# def backport_to_branches(target_branch, labels):
#     return list(
#         label2branch(label)
#         for label in labels
#         if normalize_label(label) in label2branch
#         and label2branch[label] != target_branch
#     )
