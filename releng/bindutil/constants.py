from pathlib import Path

_SCRIPT_DIR = Path(__file__).parent.parent.resolve()
TEMPLATES_DIR = _SCRIPT_DIR / "templates"
MESSAGES_DIR = _SCRIPT_DIR / "messages"

RELEASE_CHECKLIST_TEMPLATE_URL = "https://gitlab.isc.org/isc-projects/bind9/-/raw/main/.gitlab/issue_templates/Release.md"

DEVELOPERS = {
    "aram": {
        "timezone": "europe",
    },
    "artem": {
        "timezone": "europe",
    },
    "aydin": {
        "timezone": "europe",
    },
    "each": {
        "timezone": "us",
    },
    "marka": {
        "timezone": "australia",
    },
    "matthijs": {
        "timezone": "europe",
    },
    "michal": {
        "timezone": "europe",
    },
    "mnowak": {
        "timezone": "europe",
    },
    "ondrej": {
        "timezone": "europe",
    },
    "pspacek": {
        "timezone": "europe",
    },
    "stepan": {
        "timezone": "europe",
    },
    "nicki": {
        "timezone": "europe",
    },
    "andoni": {
        "timezone": "europe",
    },
}
