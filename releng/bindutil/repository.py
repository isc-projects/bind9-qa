import datetime
import logging
import os

from contextlib import contextmanager
from pathlib import Path
from types import MappingProxyType
from typing import IO, Iterator, Optional, Tuple, cast

import pygit2


class SSHKeyPairAuth(pygit2.RemoteCallbacks):

    def credentials(  # type: ignore
        self, url: str, username_from_url: Optional[str], allowed_types: int
    ) -> Optional[pygit2.KeypairFromAgent]:
        logging.info("Git repository at %s requires authentication", url)

        self._log_allowed_credentials(allowed_types)

        ssh_key = pygit2.KeypairFromAgent(str(username_from_url or ""))
        if allowed_types & ssh_key.credential_type:
            logging.info("Requesting key pair from SSH agent")
            return ssh_key

        logging.error("No supported authentication methods left")
        return None

    def _log_allowed_credentials(self, allowed_types: int) -> None:
        allowed_credentials = []

        for credential_type in pygit2.enums.CredentialType:
            if allowed_types & credential_type.value:
                allowed_credentials.append(f"GIT_CREDENTIAL_{credential_type.name}")

        logging.debug("Supported credential types: %s", ", ".join(allowed_credentials))

    def push_update_reference(self, refname: str, message: Optional[str]) -> None:
        if message:
            raise pygit2.GitError(f"Failed to push {refname}: {message}")


class SSHKeyPairAuthWithProgress(SSHKeyPairAuth):

    _OBJECT_PERCENT_PER_REPORT = 10
    _SECONDS_PER_REPORT = 10

    def __init__(self) -> None:
        super().__init__()
        self._current_percentage = 0
        self._start = datetime.datetime.now()
        self._last_report_time = self._start
        self._last_report_percentage = 0

    @property
    def _time_elapsed(self) -> datetime.timedelta:
        return datetime.datetime.now() - self._start

    def transfer_progress(self, stats: pygit2.remotes.TransferProgress) -> None:
        self._update_percentage(stats)

        if not self._report_due():
            return

        if self._last_report_percentage != 100:
            report_emitted = self._report_object_progress(stats)
        else:
            report_emitted = self._report_delta_progress(stats)

        if not report_emitted:
            return

        self._last_report_time = datetime.datetime.now()
        self._last_report_percentage = self._current_percentage

    def _update_percentage(self, stats: pygit2.remotes.TransferProgress) -> None:
        try:
            percentage = int(stats.received_objects / stats.total_objects * 100)
        except ZeroDivisionError:
            return

        assert percentage <= 100

        self._current_percentage = percentage

    def _report_due(self) -> bool:
        return self._report_due_object_percentage() or self._report_due_time()

    def _report_due_object_percentage(self) -> bool:
        current_step = self._current_percentage // self._OBJECT_PERCENT_PER_REPORT
        previous_step = self._last_report_percentage // self._OBJECT_PERCENT_PER_REPORT

        return current_step != previous_step

    def _report_due_time(self) -> bool:
        time_since_last_report = datetime.datetime.now() - self._last_report_time
        seconds_since_last_report = time_since_last_report.total_seconds()

        return seconds_since_last_report >= self._SECONDS_PER_REPORT

    def _report_object_progress(self, stats: pygit2.remotes.TransferProgress) -> bool:
        try:
            speed = int(stats.received_bytes / self._time_elapsed.total_seconds())
        except ZeroDivisionError:
            return False

        logging.info(
            "Received: %s, %d/%d objects (%d%%), elapsed: %s, speed: %s/s",
            self._format_bytes(stats.received_bytes),
            stats.received_objects,
            stats.total_objects,
            self._current_percentage,
            self._format_time_elapsed(),
            self._format_bytes(speed),
        )

        return True

    def _report_delta_progress(self, stats: pygit2.remotes.TransferProgress) -> bool:
        try:
            delta_percentage = int(stats.indexed_deltas / stats.total_deltas * 100)
        except ZeroDivisionError:
            return False

        logging.info(
            "Processed: %d/%d deltas (%d%%), elapsed: %s",
            stats.indexed_deltas,
            stats.total_deltas,
            delta_percentage,
            self._format_time_elapsed(),
        )

        return True

    def _format_bytes(self, value: int) -> str:
        thresholds = {
            1024 * 1024 * 1024: "GB",
            1024 * 1024: "MB",
            1024: "kB",
            1: "B",
        }

        for factor, unit in thresholds.items():
            if value > factor:
                return f"{value / factor:.2f} {unit}"

        return "none"

    def _format_time_elapsed(self) -> str:
        time_elapsed = self._time_elapsed

        hours = time_elapsed.seconds // 3600
        minutes = time_elapsed.seconds % 3600 // 60
        seconds = time_elapsed.seconds % 60

        return f"{hours:02d}:{minutes:02d}:{seconds:02d}"


class BIND9Repository:

    _CALLBACKS = SSHKeyPairAuthWithProgress

    def __init__(self, repo_path: str, fetch_depth: int = 0) -> None:
        self._repo = pygit2.Repository(repo_path)
        self._fetch_depth = fetch_depth
        self._set_defaults_from_environment()

    def _set_defaults_from_environment(self) -> None:
        if "user.name" not in self._repo.config:
            ci_user_name = os.environ.get("GITLAB_USER_NAME")
            if ci_user_name:
                self._repo.config["user.name"] = ci_user_name
        if "user.email" not in self._repo.config:
            ci_user_email = os.environ.get("GITLAB_USER_EMAIL")
            if ci_user_email:
                self._repo.config["user.email"] = ci_user_email

    @classmethod
    def clone(cls, url: str, repo_path: str, fetch_depth: int = 0) -> "BIND9Repository":
        pygit2.clone_repository(
            url, repo_path, depth=fetch_depth, callbacks=cls._CALLBACKS()
        )
        return BIND9Repository(f"{repo_path}/.git", fetch_depth=fetch_depth)

    @staticmethod
    def stub(url: str, repo_path: str, fetch_depth: int = 0) -> "BIND9Repository":
        repo = pygit2.init_repository(repo_path)
        repo.remotes.create("origin", url)
        return BIND9Repository(f"{repo_path}/.git", fetch_depth=fetch_depth)

    @property
    def _index(self) -> pygit2.Index:
        return self._repo.index

    @property
    def head(self) -> pygit2.Oid | str:
        return self._repo.head.target

    @property
    def user(self) -> str:
        return self._repo.config["user.email"].split("@")[0]

    def check_status(self) -> bool:
        if not self._repo.workdir or Path(self._repo.workdir) != Path.cwd():
            logging.error(
                "This script needs to be run from the root directory of a "
                "working copy of the BIND 9 Git repository"
            )
            return False

        status = self._repo.status()
        for flags in status.values():
            if flags != pygit2.GIT_STATUS_CURRENT:
                logging.error("Repository is not clean, see 'git status'")
                return False

        return True

    def add_to_index(self, path: str) -> None:
        self._index.add(path)
        self._index.write()

    def get_tag(self, tag: str, remote_name: Optional[str]) -> pygit2.Reference:
        tag_ref = f"refs/tags/{tag}"
        if remote_name:
            remote = self._repo.remotes[remote_name]
            refspec = f"{tag_ref}:{tag_ref}"
            logging.info(
                "Fetching tag '%s' from '%s' (depth: %s)",
                tag,
                remote_name,
                self._fetch_depth if self._fetch_depth else "full",
            )
            remote.fetch(
                [refspec], callbacks=self._CALLBACKS(), depth=self._fetch_depth
            )

        return self._repo.lookup_reference(tag_ref)

    def parse_branch(self, branch_with_remote: str) -> Tuple[str, str]:
        remote, branch_without_remote = branch_with_remote.split("/", maxsplit=1)
        try:
            self._repo.remotes[remote]
        except KeyError as exc:
            raise KeyError from exc
        return remote, branch_without_remote

    def remote_branch_exists(self, branch: str) -> bool:
        remote_name, branch_name = self.parse_branch(branch)
        remote_refs = self._repo.remotes[remote_name].ls_remotes()
        remote_ref_names = [r["name"] for r in remote_refs]
        remote_branch_ref = f"refs/heads/{branch_name}"
        return remote_branch_ref in remote_ref_names

    def get_remote_branch(self, branch: str, fetch: bool = False) -> pygit2.Branch:
        if fetch:
            remote_name, branch_name = self.parse_branch(branch)
            remote = self._repo.remotes[remote_name]
            remote_ref = f"refs/heads/{branch_name}"
            local_ref = f"refs/remotes/{remote_name}/{branch_name}"
            refspec = f"{remote_ref}:{local_ref}"
            logging.info(
                "Fetching branch '%s' from '%s' (depth: %s)",
                branch_name,
                remote_name,
                self._fetch_depth if self._fetch_depth else "full",
            )
            remote.fetch(
                [refspec], callbacks=self._CALLBACKS(), depth=self._fetch_depth
            )

        return self._repo.branches.remote[branch]

    def push_branch(
        self,
        remote_name: str,
        branch: Optional[str],
        push_to: Optional[str] = None,
        force: bool = False,
    ) -> None:
        if branch:
            if force:
                refspec = "+"
                action = "Force-pushing"
            else:
                refspec = ""
                action = "Pushing"
            refspec += f"refs/heads/{branch}"
            if not push_to:
                logging.info("%s '%s' to remote '%s'", action, branch, remote_name)
            else:
                refspec += f":refs/heads/{push_to}"
                logging.info(
                    "%s '%s' as '%s' to remote '%s'",
                    action,
                    branch,
                    push_to,
                    remote_name,
                )
        else:
            assert push_to
            refspec = f":refs/heads/{push_to}"
            logging.info("Deleting branch '%s' at remote '%s'", push_to, remote_name)

        remote = self._repo.remotes[remote_name]
        remote.push([refspec], callbacks=self._CALLBACKS())

    def checkout_branch(self, branch_name: str, target_branch: pygit2.Branch) -> None:
        branch = self._repo.branches.local.create(
            branch_name, target_branch.peel(1), force=True
        )
        branch.upstream = target_branch
        self._repo.checkout(branch)

    def checkout_file(
        self, path: str, revision: pygit2.Commit, must_exist: bool = True
    ) -> None:
        try:
            git_object = revision.tree / path
        except KeyError as exc:
            if must_exist:
                raise exc
            return

        with self.open_file_for_writing(path) as file:
            blob = cast(pygit2.Blob, git_object)
            file.write(blob.data)
        self.add_to_index(path)

    class UnresolvedMergeConflicts(Exception):
        pass

    @contextmanager
    def open_file_for_writing(self, path: str) -> Iterator[IO]:
        with open(self._repo.workdir + path, "wb") as file:
            yield file

    @contextmanager
    def merge(
        self, commit: pygit2.Object, commit_message: str
    ) -> Iterator[MappingProxyType[str, Tuple[bytes, bytes]]]:
        logging.info("Starting merge of %s into HEAD", commit.id)
        self._repo.merge(commit.id)

        if self._index.conflicts:
            conflicts = {}
            for ancestor, ours, theirs in self._index.conflicts:
                path = ours.path if ours else theirs.path if theirs else ancestor.path
                if ours:
                    ours_blob = cast(pygit2.Blob, self._repo[ours.id])
                    ours_data = ours_blob.data
                else:
                    ours_data = b""
                if theirs:
                    theirs_blob = cast(pygit2.Blob, self._repo[theirs.id])
                    theirs_data = theirs_blob.data
                else:
                    theirs_data = b""
                conflicts[path] = (ours_data, theirs_data)
            logging.info("Found merge conflicts in: %s", ", ".join(conflicts.keys()))
            yield MappingProxyType(conflicts)
        else:
            logging.info("No merge conflicts detected")
            yield MappingProxyType({})

        if self._index.conflicts:
            raise self.UnresolvedMergeConflicts

        logging.info("Preparing merge commit")
        self._commit_to_head(commit_message, second_parent=commit.id)
        logging.info("Merge successful, cleaning up")
        self._repo.state_cleanup()

    def commit_all(self, message: str) -> None:
        self._index.add_all()
        self._index.write()

        self._commit_to_head(message)

    def _commit_to_head(
        self, message: str, second_parent: Optional[pygit2.Oid] = None
    ) -> None:
        author = pygit2.Signature(
            self._repo.config["user.name"], self._repo.config["user.email"]
        )
        tree = self._index.write_tree()
        parents = [self._repo.head.target]
        if second_parent:
            parents.append(second_parent)
        self._repo.create_commit(
            self._repo.head.name, author, author, message, tree, parents
        )
