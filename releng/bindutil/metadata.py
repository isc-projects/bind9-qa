"""
Read metadata.json and represent dates as datetime.date objects.
"""

import datetime
import json


class ReleaseMetadata:
    def __init__(self, json_file):
        self.data = json.load(json_file)

        schedule = self.data["schedule"]
        for name, date_str in schedule.items():
            if date_str is not None:
                schedule[name] = datetime.date.fromisoformat(date_str)

        assert schedule["freeze"] < schedule["tag"] < schedule["public"]
        if schedule["asn"] is not None:
            assert (schedule["public"] - schedule["asn"]).days >= 7

    def schedule_text(self):
        sched = self.data["schedule"]
        text = f"""\
* {sched['freeze'].strftime("%b %d (%a)")} - code freeze, QA does sanity checks & docs
* {sched['tag'].strftime("%b %d (%a)")} - tagging, no changes possible (not even to docs)
"""
        if sched["asn"] is not None:
            text += f"""* {sched['asn'].strftime("%b %d (%a)")} - ASN\n"""
        text += f"""* {sched['public'].strftime("%b %d (%a)")} - public release\n"""
        return text
