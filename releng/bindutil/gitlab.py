import os

import gitlab


def get_isc_gitlab(with_write_permissions=False):
    if os.getenv("CI_SERVER_URL", None) is not None:
        url = os.getenv("CI_SERVER_URL")
        if with_write_permissions:
            token_variable = "BIND_TEAM_WRITE_TOKEN"
        else:
            token_variable = "BIND_TEAM_API_TOKEN"
        token = os.getenv(token_variable)
        if not token:
            raise EnvironmentError(f"Environment variable {token_variable} not set")
        return gitlab.Gitlab(url, token)

    return gitlab.Gitlab.from_config()  # when running locally on dev machine


def get_bind9_project(mnemonic, with_write_permissions=False):
    isc_gitlab = get_isc_gitlab(with_write_permissions)
    match mnemonic:
        case "public":
            return isc_gitlab.projects.get("isc-projects/bind9")
        case "private":
            return isc_gitlab.projects.get("isc-private/bind9")
        case _:
            raise ValueError(f"Unknown project {mnemonic}")
