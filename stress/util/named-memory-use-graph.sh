#!/usr/bin/env bash

set -e

. "${QA_STRESSDIR}/config.sh"

if [[ -z "${1}" ]]; then
	echo_t "INFO: Usage: $0 /path/to/stress/test/workspace/output/ns3/vm.txt"
	exit 1
fi

SOURCE_FILE="${1}"
if [[ -z "${MODE}" ]]; then
	echo_t "ERROR: 'MODE' variable not set."
	exit 1
fi
OS_NAME="${OS_NAME:-$(sed -nE 's|^PRETTY_NAME="(.*)"|\1 / |p' /etc/os-release 2>/dev/null || true)}"
UNAME="${UNAME:-$(uname -rs)}"
# Escape underscores so that Gnuplot does not interpret them as subscript control codes.
UNAME="${UNAME//_/\\\\_}"
if [[ -z "${NAMED}" ]]; then
	echo_t "ERROR: 'NAMED' variable not set."
	exit 1
fi
BIND_VERSION="${BIND_VERSION:-$("${NAMED}" -V | awk '/^BIND 9/ { print $1 " " $2 " " $NF }')}"
LIBUV_VERSION=$("${NAMED}" -V | sed -nE 's|^linked to libuv version: (.*)| / libuv \1|p')
NS=$(basename "$(dirname "${SOURCE_FILE}")")
DATA_FILE="${OUTDIR}/named-memory-use-graph-${NS}.data"
GRAPH_FILE="${OUTDIR}/named-memory-use-graph-${NS}.png"
TIMESTAMP=0

rm -f "${DATA_FILE}"

while read -r _ VMM RSS DIRTY; do
	echo "${TIMESTAMP}	${VMM}	${RSS}  ${DIRTY}" >> "${DATA_FILE}"
	TIMESTAMP=$((TIMESTAMP+30))
done < "${SOURCE_FILE}"

DURATION_MINUTES="$(($(wc -l < "${SOURCE_FILE}") / 2 ))"

cat <<EOF > gnuplot_command.txt
set terminal png size 1280,720
set output "${GRAPH_FILE}"
set title "{/:Bold Mode: ${MODE} / ${OS_NAME}${UNAME} / ${BIND_VERSION}${LIBUV_VERSION}}\nStress Test - Memory Use"
set xlabel "{/:Bold Time Elapsed [min]}"
set ylabel "{/:Bold Memory Used [MB]}"
set key outside right center
set grid xtics ytics
set xrange [0:${DURATION_MINUTES}]
EOF
if [[ "$(uname -s)" = "Linux" ]]; then
    cat <<EOF >> gnuplot_command.txt
plot "${DATA_FILE}" using (\$1/60):(\$2/1000) with lines title "VSZ" linecolor rgb "#306090" linewidth 3, \
     "${DATA_FILE}" using (\$1/60):(\$3/1000) with lines title "RSS" linecolor rgb "#FF0000" linewidth 3, \
     "${DATA_FILE}" using (\$1/60):(\$4/1000) with lines title "DIRTY" linecolor rgb "#40826D" linewidth 3
EOF
else
    cat <<EOF >> gnuplot_command.txt
plot "${DATA_FILE}" using (\$1/60):(\$2/1000) with lines title "VSZ" linecolor rgb "#306090" linewidth 3, \
     "${DATA_FILE}" using (\$1/60):(\$3/1000) with lines title "RSS" linecolor rgb "#FF0000" linewidth 3
EOF
fi

gnuplot gnuplot_command.txt

rm -f "${DATA_FILE}"

echo_t "INFO: Successfully wrote ${GRAPH_FILE}"
