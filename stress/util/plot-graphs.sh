#!/usr/bin/env bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

set -e

if [[ -z "${MODE}" || -z "${OUTDIR}" || -z "${QA_STRESSDIR}" ]]; then
    echo "ERROR: Some of the following variables were not set (MODE=${MODE}, OUTDIR=${OUTDIR}, QA_STRESSDIR=${QA_STRESSDIR})"
    exit 1
fi

. "${QA_STRESSDIR}/config.sh"

if command -v gnuplot >/dev/null 2>&1; then
    for ns in "${OUTDIR}"/ns*; do
        "${QA_STRESSDIR}/util/named-memory-use-graph.sh" "${ns}/vm.txt"
    done
    for latency_file in "${OUTDIR}"/latency*.txt; do
        latency_plot="$(dirname "${latency_file}")/$(basename "${latency_file}" .txt).png"
        gnuplot -c "${QA_STRESSDIR}/util/latency.gp" "${latency_file}" > "${latency_plot}"
        echo_t "INFO: latency plot '${latency_plot}' created"
    done
else
    echo_t "INFO: gnuplot not found in PATH, unable to plot graphs"
    exit 1
fi
