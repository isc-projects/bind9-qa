#!/usr/bin/env bash
#
# build-query-list - Extracts set of queries from RPZ zone files
#
# Processes the RPZ zone files listed on the command line and extracts a
# set of queries from them.
#
#   build-query-list query-file rpz-limit rpz-zone-file [rpz-zone-file...]
#
# The zones files are assumed to be labelled <zone-name>.txt and each
# record in the file is assumed to be a FQDN (i.e. the output of an AXFR).
# A maximum of <rpz-limit> random records are extracted from each one and
# <rpz-limit> random records are then taken from the result.
#
# The file is processed to that any wildcards are replaced by a random
# prefix and the result is merged with the given query file.
#
# The result is written to "allnames.lis" in the default directory.

OUTPUT_FILE="allnames.lis"
TEMP_FILE="/tmp/allnames.tmp"
rm -fr $OUTPUT_FILE $TEMP_FILE

# process-zone-file - Processes a Zone File
#
# $1    Zone file to process
# $2    Maximum number of records to extract from the file
# $3    File to which the result is to be appended

process_zone() {
    infile="$1"
    limit="$2"
    outfile="$3"

    zone=$(basename $infile .zone)

    # The following pipeline:
    # 1. Extracts all records from the file except NS and SOA records
    # 2. Removes the string '<zone-name>.host.dtq. from each record
    # 3. Sorts them into random order
    # 4. Takes the first 'limit' records
    # 5. Removes the zone name suffix
    # 6. Replaces a leading '*' with a random prefix and adds a trailing
    #    RR type

    grep '^[0-9A-Za-z\*]' $infile | grep -E -v 'NS|SOA' | \
        sed -E -e "s/$zone.*//" | \
        sort -R | \
        head -n ${limit} | \
        sed -e "s/\.${zone}\.//" | \
        awk ' \
            BEGIN   {count = 1;} \
                    { if (substr($1, 1, 1) == "*") { \
                          domain = "random" count++ substr($1, 2); \
                      } else { \
                          domain = $1; \
                      } \
                      print domain " A"; \
                    }' \
        >> $outfile
}

# Main code starts here.  First, check arguments.

if [ $# -lt 3 ]; then
    echo "ERROR: Usage: build-query-list.sh <query-file> <limit> <zone-file> [zone-file..]"
    exit 1
fi

if [ ! -e $1 ]; then
    echo "ERROR: input query file not found"
    exit 1
fi
query_file=$1
shift

test "$1" -eq "$1" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "ERROR: line limit must be numeric"
fi
limit=$1
shift

# Build up the query file we are after.

for file in "$@"
do
    echo "INFO: processing $file"
    process_zone $file $limit $TEMP_FILE
done

# Produce a random set of queries combining the input file and no more
# that $limit lines of the temporary file.

sort -R $TEMP_FILE | head -n ${limit} | cat - $query_file | sort -R > $OUTPUT_FILE

# ... and clean up

rm -f $TEMP_FILE
