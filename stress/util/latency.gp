# gnuplot command file to plot latency of query (as measured by the "Query
# Time" data in "dig" output) against time into the test.
#
# The data file is assumed to have:
#
# Column 1: Time of data point, expressed as seconds since the start of the
#           epoch.
# Column 2: "dig"-reported query time in milliseconds.

set terminal png size 1280,720
set title "{/:Bold Latency v Time for BIND9 Stress Test}"
set xlabel "{/:Bold Time (seconds into test)}"
set ylabel "{/:Bold Latency (ms)}"
set key outside right center
set grid xtics ytics
stats ARG1 using 1:2 nooutput
plot ARG1 using ($1 - STATS_min_x):2 with lines notitle
