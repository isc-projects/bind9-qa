/*
 * Copyright (C) 2017  Internet Systems Consortium, Inc. ("ISC")
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * create-query-file - Create Query File
 *
 * Create a file of names suitable for stress testing.  The names comprise
 *
 * 1) Names known to be in test.example. (max 100k).  These names are of
 *    the form nnnnnn.test.example.
 * 2) Names in test.example. domain but not in the zone (max 100k).  These
 *    names are of the form invalidnnnnnn.test.example.
 * 3) Names from the Spamhaus RPZ zones (max equal to size of file).  If the
 *    name read from the file starts with an asterisk, the asterisk is
 *    replaced with a number.
 *
 * Invocation:
 *  create-query-file [-r rpznames] nKnown nUnknown [nRpz]
 *
 * -r rpznames      Name of the file holding names in RP zones.  If absent,
 *                  the third parameter (if present) is ignored and the
 *                  file comprises known and unknown names only.
 *
 * nKnown           Number of known names (i.e. nnnnnn.test.example.)
 *
 * nUnknown         Number of unknown names (invalidnnnnnn.test.example.)
 *
 * nRpz             Number of RPZ names.  Ignored if the rpznames file is
 *                  not supplied.  Defaults to the number of names in the
 *                  file.
 *
 * Output is written to stdout.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>

/* Numbers of records from each source */
static long nknown = -1;
static long nunknown = -1;
static long nrpz = -1;

/* RPZ file */
static const char *rpzfile = NULL;

/*
 * usage - Print Usage Information
 */

void
usage() {
    printf("create-query-file [-r rpznames] nKnown nUnknown [nRpz]\n");
}


/*
 * Parse arguments.  The results are placed in the appropriate static
 * variables.
 */

long
parse_int(const char* string) {
    char    *endptr;    /* Pointer to end of string */
    long    result;     /* Integer value of string */

    if ((! string) || (! *string)) {
        fprintf(stderr, "Internal error: string passed to parse_int is null\n");
        exit(1);
    }

    result = strtol(string, &endptr, 10);
    if (*endptr != '\0') {
        fprintf(stderr, "Error: %s is not a valid number\n", string);
        exit(1);
    } else if (result < 0) {
        fprintf(stderr, "Error: all numeric arguments must be >= 0\n");
        exit(1);
    }

    return (result);
}

void
parse_args(int argc, char **argv) {
    int     ch;     /* getopt return */

    /* Process command-line switches */
    while ((ch = getopt(argc, argv, "hr:")) != -1) {
        switch (ch) {
        case 'r':
            rpzfile = strdup(optarg);
            break;

        case 'h':
            usage();
            exit(0);

        case '?':
        default:
            usage();
            exit(1);
        }
    }
    argc -= optind;
    argv += optind;

    /* Check the remaining values */
    if ((argc < 2) || (argc > 3)) {
        usage();
        exit(1);
    }

    nknown = parse_int(argv[0]);
    if (nknown > 100000) {
        printf("Warning: nKnown > 100,000, will be limited to 100,000\n");
        nknown = 100000;
    }

    nunknown = parse_int(argv[1]);
    if (nunknown > 100000) {
        printf("Warning: nUnknown > 100,000, will be limited to 100,000\n");
        nunknown = 100000;
    }

    if (argc == 3) {
        nrpz = parse_int(argv[2]);
    }
}

/* Print known/unknown queries */
void
print_queries(const char *prefix, long limit) {
    long    i;      /* Loop counter */

    for (i = 0; i < limit; ++i) {
        printf("%s%06ld.test.example. A\n", prefix, i);
    }
}


/* Queries from the RPZ file */
void
print_rpz_queries(void) {
    FILE    *fp;            /* RPZ file pointer */
    long    i;              /* Loop counter */
    char    buffer[65536];  /* Input buffer */
    char    *result;        /* fgets status return */
    char    *lfpos;         /* Position of line feed */
    int     astcnt = 0;     /* Number of asterisks found */

    fp = fopen(rpzfile, "r");
    if (!fp) {
        perror("Error: unable to open RPZ file");
        exit(1);
    }

    for (i = 0; i < nrpz; ++i) {
        result = fgets(buffer, sizeof(buffer), fp);
        if (result != NULL) {
            /* Got something.  Strip the LF and output */
            buffer[sizeof(buffer) - 1] = '\0';
            lfpos = strstr(buffer, "\n");
            if (lfpos) {
                *lfpos = '\0';
            }
            /*
             * If the first character is an asterisk, replace it by the
             * number of asterisks found so far.
             */
            if (buffer[0] == '*') {
                printf("%d%s A\n", ++astcnt, &buffer[1]);
            } else {
                printf("%s A\n", buffer);
            }
        } else if (feof(fp)) {
            break;
        } else {
            perror("Error reading RPZ file");
            exit(1);
        }
    }

    (void) fclose(fp);
}
            

/* Main function */
int
main(int argc, char** argv) {
    parse_args(argc, argv);

    print_queries("", nknown);
    print_queries("invalid", nunknown);
    if (rpzfile) {
        if (nrpz < 0) {
            nrpz = LONG_MAX;
        }
        print_rpz_queries();
    }
        
    return 0;
}
