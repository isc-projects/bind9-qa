/*
 * Copyright (C) 2017  Internet Systems Consortium, Inc. ("ISC")
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * create_zone - Create Zone with A Records
 *
 * This is a small utility program to for creating zones.  It creates a zone
 * with one NS record (ns2) and a number of A records.  Each "A" record is
 * named "<n>" (where <n> is a unique six-digit number) and each is assigned
 * a unique IPv4 address, from 11.1.1.1 upwards. (Note though, that in each
 * octect of the address, "1" and "255" are never used.)
 *
 * Usage:
 *  create_zone [-n nsaddr] zone-name [count]
 *
 * -n nsaddr    Address of the single nameserver for the zone.  If not
 *              given, 10.53.0.1 will be used.
 *
 * zone-name    Name of the zone.  A trailing period will be appended to the
 *              name if not already present to ensure that the name is
 *              an absolute name.
 *
 * count        Number of A records to write to the zone.  If not specified,
 *              no records are written: the zone comprises the SOA record, the
 *              NS record, and its glue record.
 *
 * Output is written to stdout.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * usage - Print Usage Information
 */

void
usage() {
    printf("Usage: create_zone [-n nsaddr] zone-name [count]\n");
    printf("\n");
    printf("-n nsaddr   Nameserver address.  Defaults to 10.53.0.1\n");
    printf("\n");
    printf("zone-name   Name of the zone\n");
    printf("count       Number of A records in the zone.  This may be 0.\n");
}

/*
 * header - Print Header of Zone
 *
 * Prints the zone header (i.e. the empty zone).
 *
 * Arguments:
 *      zone - Name of the zone.
 */

void
header(const char* zone, const char* nsaddr) {
    printf("$ORIGIN %s\n", zone);
    printf("$TTL 300\n");
    printf("%s IN SOA ns2.%s hostmaster.%s (\n", zone, zone, zone);
    printf("            2000042407 ; serial\n");
    printf("            20         ; refresh (20 seconds)\n");
    printf("            20         ; retry (20 seconds)\n");
    printf("            1814400    ; expire (3 weeks)\n");
    printf("            3600       ; minimum (1 hour)\n");
    printf("            )\n");
    printf("\n");
    printf("      NS ns2\n");
    printf("\n");
    printf("ns2   A  %s\n", nsaddr);
}


/*
 * body - Print Body of Zone
 *
 * Prints all the A records in the zone.
 *
 * Arguments:
 *      count - Number of host records to write.
 */

void
body(int count) {
    int n1, n2, n3, n4;     /* Counters */
    int host = 0;           /* Number of record being output */

    if (count > 0) {

        /*
         * At least one A record will be printed, so include a space after
         * the header.
         */
        printf("\n");

        /*
         * Print the records. The NS glue records were put in 10.x.x.x.  We
         * put all the host records in 11.x.x.x up. (No particular reason.)
         */
        for (n1 = 11; n1 < 255; ++n1) {
            for (n2 = 1; n2 < 255; ++n2) {
                for (n3 = 1; n3 < 255; ++n3) {
                    for (n4 = 1; n4 < 255; ++n4) {
                        printf("%6.6d  A  %d.%d.%d.%d\n",
                               host, n1, n2, n3, n4);
                        ++host;
                        if (host >= count) {
                            return;
                        }
                    }
                }
            }
        }
    }
}


/*
 * main - Main Program
 *
 * Parse the command line arguments and produce the zone.
 */

int
main(int argc, char** argv) {
    char zone[512];                     /* Zone name - more than enough space */
    int zonelen = 0;                    /* Length of the zone name */
    long host_count = 0;                /* Number of A records */
    char *endptr = NULL;                /* End of string processed */
    const char* nsaddr = "10.53.0.1";   /* Nameserver address */
    int ch;                             /* Command switch character */

    /* Process switches */
    while ((ch = getopt(argc, argv, "n:")) != -1) {
        switch (ch) {
            case 'n':
                nsaddr = optarg;
                break;

            case '?':
            case 'h':
            default:
                usage();
                return 0;
        }
    }
    argc -= optind;
    argv += optind;
    
    /* Check the number of arguments */
    if ((argc < 1) || (argc > 2)) {
        usage();
        return 1;
    }

    /*
     * Ensure that the last character in the zone name is a ".". To do this,
     * take a copy of the zone.
     */
    strncpy(zone, argv[0], sizeof(zone));
    zone[sizeof(zone) - 1] = '\0';

    /* ... and append a . if needed and there is enough space in the buffer. */
    zonelen = strlen(zone);
    if (zonelen < sizeof(zone) - 1) {
        if (zone[zonelen - 1] != '.') {
            zone[zonelen] = '.';
            zone[zonelen + 1] = '\0';
        }
    }

    /* How many A records? */
    if (argc == 2) {
        host_count = strtol(argv[1], &endptr, 10);
        if (*endptr != '\0') {
            /* Invalid count string */
            usage();
            return 2;
        }
    }

    /* Print the header */
    header(zone, nsaddr);


    /* Now all the hosts. */
    body(host_count);
    

    return 0;
}
