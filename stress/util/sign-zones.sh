#!/bin/bash
#
# sign-zones.sh - Sign Zones
#
# This script takes the unsigned zones in the ns1 and ns2 directories and
# signs them.  It then updates the trust-anchors clause in ns3 (the resolver)
# with the key for the pseudo-root zone.
#
# This script is not used in tests: it is a utility to re-sign the zones should
# they be altered for any reason.
#
# It is assumed that:
#
# 1) ns1, ns2 and ns3 are all subdirectories of the directory holding this
#    script.
# 2) ns1 contains the "root" zone in a file called root.db.
# 3) ns2 contains the "test.example." zone in a file called test.example.db.
# 4) ns3 contains the resolver and has a 'include "trust-anchors.conf"' statement
#    in its configuration file.
#
# Invocation:
#       sign-zones.sh
#
# Environment Variables:
# DNSSEC_KEYGEN         Path to dnssec-keygen.  If not defined, it will be
#                       assumed that dnssec-keygen is already in the path.
# DNSSEC_SIGNZONE       Path to dnssec-signzone.  If not defined, it will be
#                       assumed that dnssec-signzone is already in the path.
#
# Inputs:
# ns1/root.db           The "root" zone file.
# ns2/test.example.db   The zone file for the zone "test.example."
#
# Outputs:
# ns1/root.db.signed    Signed copy of the root zone.  This will include the
#                       DS record of test.example.
# ns1/keys              Directory holding the relevant keys for the "root"
#                       zone.
# ns2/test.example.db.signed
#                       Signed copy of the "test.example." zone
# ns2/keys              Directory holding the keys used to sign the test.example
#                       zone.
# ns2/keys/trust-anchors.conf
#                       A trust-anchors clause referring to the private root zone.
#                       (As this is a completely private system, we don't want to
#                       use RFC5011 to check for new root keys.)
# ns3/keys/trust-anchors.conf
#                       A trust-anchors clause referring to the private root zone.
#                       (As this is a completely private system, we don't want to
#                       use RFC5011 to check for new root keys.)

# Ensure default directory is where this script is located.
cd `dirname $0`

# Check for existence of required programs.
signzone=${DNSSEC_SIGNZONE:=dnssec-signzone}
if [ "`which $signzone`" = "" ]; then
    echo "ERROR: unable to find key generation program $signzone"
    exit 1
fi

keygen=${DNSSEC_KEYGEN:-dnssec-keygen}
if [ "`which $keygen`" = "" ]; then
    echo "ERROR: unable to find key generation program $keygen"
    exit 1
fi

# Ensure that the keys directories do not already exist.
for directory in ns1/keys ns2/keys ns3/keys ; do
    if [ -e $directory ]; then
        echo "ERROR: key directory $directory already exists"
        exit 1
    fi
done


#
# Arguments:
#   $1  Zone name
#   $2  Zone file.  The output is put in a file of the same name with
#       ".signed" appended.
#   $3  Optional.  Name of the file containing DS records to add to the
#       zone before signing.
#
# On entry, the default directory is assumed to be the directory holding the
# zone file.
#
# On exit, the following are true:
#   * The signed zone is held in the ".signed" file
#   * The keys used to generate it are in the "keys" subdirectory.  This also
#     holds a file stating which key is the KSK and which is the ZSK

sign_zone() {
    # Clear up from any previous incomplete run.
    rm -fr keys
    rm -f  *.tmp
    rm -f  *.signed
    rm -f  dsset*

    zone=$1
    shift
    zonefile=$1
    shift

    # Get a symbols for the directory holding the keys
    mkdir -p keys
    cd keys
    local keydir=`pwd`
    cd ..

    echo "INFO: creating KSK for zone \"${zone}\""
    local ksk=`$keygen -K keys -a RSASHA256  -b 2048 -f KSK -L 300 $zone  | \
        tail -1`
    local ksk_id=`echo $ksk | cut -f3 -d'+' | cut -f1 -d'.'`
    local ksk_public="${keydir}/${ksk}.key"
    local ksk_private="${keydir}/${ksk}.private"
    echo "$ksk is the key-siging key" > $keydir/README.txt

    # Make the KSK as a "trusted" key.  This is only of use for the root zone,
    # but having the KSK as a known file name simplifies test setup.
    cp $ksk_public ${keydir}/trusted.key

    echo "INFO: creating ZSK for zone \"${zone}\""
    local zsk=`$keygen -K keys -a RSASHA256  -b 1024        -L 300 $zone \
        | tail -1`
    local zsk_id=`echo $zsk | cut -f3 -d'+' | cut -f1 -d'.'`
    local zsk_public="${keydir}/${zsk}.key"
    local zsk_private="${keydir}/${zsk}.private"
    echo "$zsk is the zone-siging key" >> $keydir/README.txt

    # Append the keys to the zone.
    cp $zonefile ${zonefile}.tmp
    cat $ksk_public $zsk_public >> ${zonefile}.tmp

    # Append any DS records to the zone
    if [ "$1" != "" ]; then
        cat $* >> ${zonefile}.tmp
    fi

    # Sign the zone.  Signatures will be valid from the start of 2018 through
    # to the end of 2217 - 200 years - so there is no danger of signature
    # expiration while this test is being actively run.
    echo "INFO: signing zone \"${zone}\""
    $signzone -K $keydir -s 20180101000000 -e 22171231235959 -S \
        -o $zone -f ${zonefile}.signed ${zonefile}.tmp

    # ... and tidy up.
    rm *.tmp
}


# Stage 1.  Sign "test.example.", the zone hosted by ns2.  This is done first
# so as to generate the DS records to go into the root zone.
echo "INFO: generating keys and signing test.example. zone (ns2)"
cd ns2
sign_zone test.example. test.example.db
dsfile=`pwd`/`echo dsset*`      # Get Name of file containing DS record
cd ..

# Stage 2. Sign the "root" zone, the zone hosted by ns1.  Pass to it the
# name of the file containing the DS records of test.example.
echo "INFO: generating keys and signing 'root' zone (ns1)"
echo $dsfiles
cd ns1
sign_zone . root.db $dsfile
cd ..

# Stage 3.  Put the root zone public key into the "trust-anchors.conf" file
# for the resolver.
echo "INFO: updating auth server and resolver with 'root' zone trust anchors"
awk -f make-trust-anchors.awk ns1/keys/trusted.key > ns2/trust-anchors.conf
cp ns2/trust-anchors.conf ns3/trust-anchors.conf
