#!/usr/bin/env bash

set +xe

export LC_ALL=en_US.UTF-8

#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

# stress.sh - Main Script Controlling Stress Test
#
# Sets up three nameservers (in appropriately named directories):
#
# ns1   on 10.53.0.1 and fd92:7065:b8e:ffff::1.  This is a "root" server, used
#       in bootstrapping the other servers.
#
# ns2   on 10.53.0.2 and fd92:7065:b8e:ffff::2.  This is the authoritative
#       server.
#
# ns3   on 10.53.0.3 and fd92:7065:b8e:ffff::3.  This is the recursive server.
#       It will query the server in ns2 for domains in "test.example.".
#
# ns4   on 10.53.0.4 and fd92:7065:b8e:ffff::4.  This is a server that
#       serves a zone, implements RPZ, and allows queries to the outside
#       world.
#
# ... then starts a number of processes running the NSONE "flamethrower" (see
# https://github.com/DNS-OARC/flamethrower.git) to fire queries at them.
# Packet generators started depend on the PROTOCOL environmental variable,
# e.g. UDP/v4, UDP/v6, DoH/v4, and DoH/v6 for PROTOCOL="udp doh".
#
# Arguments:
# Implicit Inputs:
# Although a number of environment variables are set in the associated
# "config.sh" script, some are set in the environment.  This allows them to
# be set from a Jenkins job. These variables are:
#
# MODE      Either "authoritative", "recursive" or "rpz".
#           This controls whether the queries are set to the authoritative
#           server, the recursive server, or the recursive server configured
#           to received RPZ transfers from Spamhaus.
#       
# PROTOCOL  Controls which packet generators are started. Defaults to "udp tcp".
#           Can be either "udp", "tcp", "doh", or "dot" or a pair of "udp"
#           and one of the TCP protocols, e.g. PROTOCOL="udp doh".
#
# RATE      Generator packet send rate.  The interpretation of this figure
#           depends on the generator: for the moment, it is the number of
#           packets sent in a 1ms interval.  The overall rate needs to be
#           multiplied by the number of generators.  The default is 50000.
#
# RELOAD_INTERVAL
#           If defined and a non-negative integer, the script spawns a process
#           which issues "rndc reload" commands to the server under test every
#           RELOAD_INTERVAL seconds.
#
# RUN_TIME  Length of time in minutes that the job should run (default 720 minutes)
#
# AUTHORITATIVE_OPTIONS
#           If specified, these options are placed into the configuration file
#           for the authoritiative server (ns2).
#
# RECURSIVE_OPTIONS
#           If specified, these options are placed into the configuration file
#           for the recursive servers (ns3 and ns4).
#
# COMMAND_SWITCHES
#           Command switches passed to all named servers, in addition to
#           "-f -c <config-file>"

# Set QA_STRESSDIR to the directory holding the scripts.
curdir=$PWD
cd $(dirname $0)
QA_STRESSDIR=$PWD
export QA_STRESSDIR
cd $curdir

# Set the environment variables and define echo_t (which adds the date and time
# to a line being output).
if [[ ! -e $QA_STRESSDIR/config.sh ]]; then
    echo "ERROR: could not find config.sh in $PWD"
    exit 1
fi
. $QA_STRESSDIR/config.sh

# Names for which a monitor process should be spawned to query for.  Query
# delays will be written to files latencyN.txt (N starting at 1), which
# correspond to positions in the QUERY_NAMES variable.
QUERY_NAMES="000007.test.example. abcd123-example.com."

# Set IPv4 and IPv6 addresses of the server under test, and also its number
# (i.e. the directory holding the files is ns<numbr>.)
NSNUM=
if [[ "$MODE" == "authoritative" ]]; then
    NSNUM=2
elif [[ "$MODE" == "recursive" ]]; then
    NSNUM=3
elif [[ "$MODE" == "rpz" ]]; then
    NSNUM=4
else
    echo_t "ERROR: environment variable MODE must be either 'authoritative', 'recursive' or 'rpz' - it is '$MODE'"
    exit 1
fi
export MODE

ADDRESS4="10.53.0.$NSNUM"
export ADDRESS4

ADDRESS6="fd92:7065:b8e:ffff::$NSNUM"
export ADDRESS6

echo_t "INFO: $MODE mode selected, server under test is ns$NSNUM, addresses $ADDRESS4 and $ADDRESS6"

# Set the port to use.
export PORT=${PORT:-5300}
export HTTPSPORT=${HTTPSPORT:-4430}
export TLSPORT=${TLSPORT:-8530}

PROTOCOL=${PROTOCOL:-udp tcp}
protocol_error() {
    echo_t "ERROR: invalid protocol (or combination of protocols) in PROTOCOL environmental variable ('$PROTOCOL')"
    exit 1
}
USE_TCP=0
USE_UDP=0
for p in $PROTOCOL; do
    if [[ "$p" =~ ^(tcp|doh|dot)$ ]]; then
        USE_TCP=$((USE_TCP+1))
    elif [[ "$p" == "udp" ]]; then
        USE_UDP=$((USE_UDP+1))
    else
        protocol_error
    fi
done
if [[ "$USE_TCP" -gt 1 ]] || [[ "$USE_UDP" -gt 1 ]]; then
    protocol_error
fi

# Generator packet send rate
RATE=${RATE:-50000}
test "$RATE" -eq "$RATE" 2> /dev/null
if [[ $? -ne 0 || $RATE -lt 0 ]]; then
    echo_t "ERROR: environment variable RATE must be zero or a positive integer - it is '$RATE'"
    exit 1
fi
echo_t "INFO: generator send rate is $RATE"

# Length of the run.
RUN_TIME=${RUN_TIME:-720}
test "$RUN_TIME" -eq "$RUN_TIME" 2> /dev/null
if [[ $? -ne 0 || $RUN_TIME -le 0 ]]; then
    echo_t "ERROR: environment variable RUN_TIME must be a positive integer - it is '$RUN_TIME'"
    exit 1
fi
echo_t "INFO: run time will be $RUN_TIME minutes"

# PID_FILES is the list of PID files for active processes.  This is used later on.
PID_FILES=
export PID_FILES

generate_backtrace() {
    while read -r core; do
        backtrace_file="${core}-backtrace.txt"
        echo_t "ERROR: Core dump file $core found"
        gdb -batch -ex "bt" -core "$core" -- "$NAMED" 2>/dev/null | sed -n '/^Core was generated by/,$p'
        echo_t "ERROR: Complete backtrace to be found in: $backtrace_file"
        gdb -batch -ex "thread apply all bt full" -core "$core" -- "$NAMED" > "$backtrace_file" 2>&1
        result=$((result + 1))
    done < <(find "$OUTDIR" \( -name 'core' -or -name 'core.*' -or -name '*.core' \))
}

kill_processes() {
    local status=0
    local termopt="${1}"
    local returnstatus="${2:-}"
    for pid in $(cat $PID_FILES 2>/dev/null); do
        if kill -0 "$pid" 2>/dev/null; then
            if [ "${returnstatus}" = "returnstatus" ]; then
                echo_t "INFO: terminating following process ${pid}:"
                ps -o command -p ${pid}
                status=$((status + 1))
                kill "$termopt" "$pid" || true
                if [ "${termopt}" = "-SIGABRT" ]; then
                    echo_t "INFO: Wait for core dumping of process ${pid} to finish"
                    sleep 60
                fi
            else
                kill "$termopt" "$pid" || true
            fi
        fi
    done
    return "${status}"
}

kill_all_processes() {
    echo_t "INFO: terminating processes: $(cat $PID_FILES | tr '\n' ' ')"
    kill_processes "-SIGTERM"
    sleep 60
    echo_t "INFO: aborting remaining processes (if any)"
    kill_processes "-SIGABRT" "returnstatus" || result=$?
}

kill_all_processes_and_quit() {
    kill_all_processes
    generate_backtrace
    exit 1
}
trap kill_all_processes_and_quit SIGINT

# Function - wait for nameserver to start
#
# Scans the output "named.log" file for a configurable interval looking for the
# the line that ends with "running", which signifies that the server has
# started.  named.log is assumed to be in the default directory.
#
# $1    Interval to wait for (default 10 seconds)

wait_for_server_start() {
    local timeout=$1
    if [[ "$timeout" == "" ]]; then
        timeout=10
    fi

    while [[ $timeout -gt 0 ]]
    do
        timeout=$((timeout - 1))
        if [[ -e named.log ]]; then
            grep " running$" named.log > /dev/null 2>&1
            if [[ $? -eq 0 ]]; then
                # String found, so server is running.  Add the PID file to the
                # list of PID files.
                PID_FILES="$PWD/named.pid $PID_FILES"
                export PID_FILES
                return
            else
                echo_t "INFO: waiting for server to complete startup, $timeout attempt(s) left"
            fi
        else
            echo_t "INFO: waiting for named.log to appear, $timeout attempt(s) left"
        fi
        sleep 1
    done

    echo_t "ERROR: did not find start message in server log"
    kill_all_processes_and_quit
}


# Function - Check if Process is Running
#
# Checks if the process identified by the passed PID file is running.
#
# Arguments:
# $1    PID file
# $2    Name of the process (used in error message)
#
# Returns:
# 0     Success, processes is running
# 1     Error, process not running or file not found

check_process() {
    # Check parameter
    if [[ "$1" == "" ]]; then
        echo_t "FATAL: check_process must be called with name of PID file"
        exit 1
    elif [[ ! -e $1 ]]; then
        echo_t "ERROR: PID file $1 does not exist"
        kill_all_processes_and_quit
    fi
    file="$1"
    pid=$(cat $file)
    ps -p $pid > /dev/null
    if [[ $? -ne 0 ]]; then
        echo_t "ERROR: process with PID file $file (pid = $pid) is no longer running"
        kill_all_processes_and_quit
    fi
}


# Function - Check if All Processes are Running
#
# Uses the list of PID files in the PID_FILES variable to see if all listed
# processes are running.  The function will terminate the script with an error
# if they aren't.

check_all_processes() {
    if [[ "$PID_FILES" == "" ]]; then
        return
    fi

    for file in $PID_FILES ; do
        check_process $file
    done
}


# Function - Start Server
#
# Starts an instance of named. The configuration file is processed to
# replace elements that are server-specific.
#
# Arguments:
# $1    Nameserver ID.

start_server() {
    # Create the working directory.  This holds everything - output log, zone
    # files etc.
    local nsid
    nsid=$1
    local nsdir
    nsdir="ns$nsid"

    cp -R $QA_STRESSDIR/$nsdir $OUTDIR

    local workdir="$OUTDIR/$nsdir"
    cd $workdir

    # Process the configuration template file if present.
    if [[ -e named.conf.in ]]; then
        sed -e "s#@AUTHORITATIVE_OPTIONS@#${AUTHORITATIVE_OPTIONS}#" \
            -e "s#@DIRECTORY@#${workdir}#" \
            -e "s#@HTTPSPORT@#${HTTPSPORT}#" \
            -e "s#@TLSPORT@#${TLSPORT}#" \
            -e "s#@PORT@#${PORT}#" \
            -e "s#@RECURSIVE_OPTIONS@#${RECURSIVE_OPTIONS}#" \
            named.conf.in > named.conf
    fi

    # Generate/sign the zone files.  Doing this here means we don't have to
    # checkout large signed files.
    if [[ "$nsdir" == "ns1" ]]; then
        # Root zone.  Just sign the "root.db" file.
        echo_t "INFO: signing the root zone file"
        $DNSSEC_SIGNZONE -S -K keys -o . root.db
	# Add trust anchors for this root
	awk 'BEGIN { print "trust-anchors {" }
	     { print $1, "static-ds", $4, $5, $6, "\"" $7 $8 "\";" }
	     END { print "};" }' < dsset-. >> named.conf
    elif [[ "$nsdir" == "ns2" ]]; then
        # test.example zone: generate the zone file and sign it.
        echo_t "INFO: creating and signing the test.example zone file"
        $CC -o ./create-zone $QA_STRESSDIR/util/create-zone.c
        ./create-zone -n 10.53.0.2 test.example. $ZONE_SIZE > test.example.db
        $DNSSEC_SIGNZONE -S -K keys -o test.example. test.example.db
    fi

    echo_t "INFO: checking ${nsdir}/named.conf validity"
    if ! $NAMED_CHECKCONF named.conf; then
        echo_t "ERROR: named-checkconf reports ${nsdir}/named.conf config file is invalid"
        exit 1
    fi

    # ... and start the server.
    echo_t "INFO: starting named on 10.53.0.${nsid}/fd92:7065:b8e:ffff::${nsid} (directory ${nsdir})"
    $NAMED $COMMAND_SWITCHES -f -c ./named.conf > ./named.run 2>&1 &
    wait_for_server_start 60
}


# Function - Start Packet Generator
#
# Starts an instance of the query generator.  In this case, it is the NS1
# program "flamethrower" (hosted by OARC).  The process is run indefinitely
# until it is killed.
#
# Queries are taken from $QA_STRESSDIR/query_datafile, which has valid A
# queries for the loaded zone and about 10% of queries giving NXDOMAINs.
# This has been supplemented by additional names in RP zones if testing
# RPZ.
#
# A "generator<n>" directory is created in $OUTDIR holding the log file and
# a PID file that can be used to terminate the process.
#
# Arguments:
# $1    Number of the directory (the "n" in generator<n>)
# $2    Either "udp" or "tcp" to indicate the protocol to use.
# $3    Either inet or inet6 for IPv4 or IPv6 queries.

start_generator() {
    # Set target directory for output
    local directory=$OUTDIR/generator$1
    local protocol=$2
    local family=$3
    local address=
    if [[ "$family" == "inet" ]]; then
        address="${ADDRESS4}"
    elif [[ "$family" == "inet6" ]]; then
        address="[${ADDRESS6}]"
    fi
    if [[ "$protocol" == "doh" ]]; then
        address="${address}/dns-query"
        target_port="$HTTPSPORT"
    elif [[ "$protocol" == "dot" ]]; then
        target_port="$TLSPORT"
    else
        target_port="$PORT"
    fi
    # Name of the query file.
    local qfile
    qfile=$OUTDIR/query_datafile

    # All verified, create the directory, start process and store pid.
    echo_t "INFO: starting generator #$1 ($protocol) on $address in $directory"
    echo_t "INFO: (using query file $qfile)"
    mkdir -p $directory
    cd $directory
    $FLAME --dnssec -P $protocol -F $family -g file -f $qfile \
        -Q $RATE -p $target_port -v 99 $address > generator.log 2>&1 &
    echo $! > $directory/generator.pid

    PID_FILES="$directory/generator.pid $PID_FILES"
    export PID_FILES
}

# Function - Start Reload
#
# Starts a background process that issues rndc reload commands to
# the server under test.
#
# The PID of the background process is 
# a PID file that can be used to terminate the process.
#
# Arguments:
# $1    Number of server being tested (i.e. files are in ns$1)
# $2    If present, interval between rndc command (in seconds)

start_reload() {
    # Check for existence of RNDC config file in the nameserver directory
    local rndcdir
    rndcdir=$OUTDIR/ns$1
    local rndcfile
    rndcfile=$rndcdir/rndc.conf

    if [ ! -e $rndcfile ]; then
        echo_t "FATAL: can't find RNDC configuration file $rndcfile"
        exit 1
    fi

    # Start the test.
    echo_t "INFO: starting process to issue reloads to ns$1"
    $QA_STRESSDIR/reload.sh $rndcfile $2 > $rndcdir/rndc-output.txt &
    echo $! > $rndcdir/reload.pid
    PID_FILES="$rndcdir/reload.pid $PID_FILES"
}

# Print the number provided with thousands separators inserted. If the input is
# not a number, just return it verbatim.
group_digits() {
    if printf "%'d" "${*}" >/dev/null 2>&1; then
        printf "%'d" "${*}"
    else
        echo "${*}"
    fi
}

convert_minutes() {
    local hours=$((${1} / 60))
    local minutes=$((${1} % 60))
    if [[ "${hours}" -gt 0 ]]; then
        printf "%d hours " "${hours}"
    fi
    printf "%d minutes\n" "${minutes}"
}

# Main logic starts here.
echo_t "INFO: dumping environment variables for diagnostic purposes"
env | sort

# Start the servers.  This creates the working directories as needed.
echo_t "INFO: all run-related files will be placed in $OUTDIR"
mkdir -p $OUTDIR

# Start servers depending on options chosen.
if [[ "$MODE" != "rpz" ]]; then
    start_server 1  # "Root" server
    server_output_dirs="$OUTDIR/ns1"
    start_server 2  # Authoritative server
    server_output_dirs="$server_output_dirs $OUTDIR/ns2"
    if [[ "$MODE" == "recursive" ]]; then
        start_server 3  # Standard recursive server
        server_output_dirs="$server_output_dirs $OUTDIR/ns3"
    fi
else
    start_server 4  # Server configured with RPZ
    server_output_dirs="$server_output_dirs $OUTDIR/ns4"
fi

# Start the statistics gathering in the background.  We pass to it the
# directories whose nameserver we want it to record.  The statistics are
# placed in the server directories.
echo_t "INFO: starting statistics collector"
$QA_STRESSDIR/get-stats.sh $server_output_dirs &
echo $! > $OUTDIR/stats.pid
PID_FILES="$OUTDIR/stats.pid $PID_FILES"
export PID_FILES

# Start the generators in the background if a non-zero rate is specified.
if [[ $RATE -gt 0 ]]; then

    # First, define the names being used.  This is the standard set of names
    # (names in the zone served by ns2 plus 5% invalid names) for authoritative
    # and recursive testing.  It is supplemented by a further 1% of names taken
    # from the response policy zones if testing RPZ.
    if [[ "$MODE" = "rpz" ]]; then
        echo_t "INFO: acquiring list of names in response policy zones"
        rm -f $OUTDIR/rpz_names.sorted
        for try in {1..5}; do
            if ! $QA_STRESSDIR/get-rpznames.sh; then
                echo_t "WARNING: Sleep for 60 seconds and retry RPZ names fetch ($try/5)"
                sleep 60
            fi
        done
        if [[ ! -s $OUTDIR/rpz_names.sorted ]]; then
            echo_t "ERROR: Getting RPZ names failed, file '$OUTDIR/rpz_names.sorted' is empty."
            kill_all_processes_and_quit
        fi
        # Randomize the order so that names from different RP zones will be
        # included in the query file.
        sort -R $OUTDIR/rpz_names.sorted > $OUTDIR/rpz_names
    fi

    echo_t "INFO: creating the query file"
    $CC -o $OUTDIR/create-query-file $QA_STRESSDIR/util/create-query-file.c
    if [[ "$MODE" = "rpz" ]]; then
        cp "$QA_STRESSDIR/../queries/70k_flame.txt" "$OUTDIR/query_datafile.tmp"
        ZONE_SIZE=$(wc -l < "$OUTDIR/query_datafile.tmp")
        "$OUTDIR/create-query-file" -r "$OUTDIR/rpz_names" 0 0 $((ZONE_SIZE / 10)) \
            > "$OUTDIR/rpz_query_datafile.tmp"
        random_query=$(shuf -n 1 "$OUTDIR/query_datafile.tmp" | awk '{ print $1 }')
        random_rpz_query=$(shuf -n 1 "$OUTDIR/rpz_query_datafile.tmp" | awk '{ print $1 }')
        QUERY_NAMES="$random_query $random_rpz_query"
        cat "$OUTDIR/rpz_query_datafile.tmp" >> "$OUTDIR/query_datafile.tmp"
    else
        "$OUTDIR/create-query-file" $ZONE_SIZE $((ZONE_SIZE / 20)) \
            > "$OUTDIR/query_datafile.tmp"
    fi
    sort -R "$OUTDIR/query_datafile.tmp" > "$OUTDIR/query_datafile"

    # Start the generators.
    count=0
    for protocol in $PROTOCOL; do
        for inet_family in inet inet6; do
            echo_t "INFO: starting $protocol $inet_family generator"
            start_generator $count "$protocol" "$inet_family"
            count=$((count + 1))
        done
    done
else
    echo_t "WARNING: rate set to 0, no generators started"
fi

# Start the latency monitoring in the background.  To avoid uncertainties
# caused by checking multiple names, we'll query for the same name over and
# over again. We actually start up two latency monitoring processes, one
# for "000007.test.example" a "random" name from the known zone, and one
# for "abcd123-example.com", a known name from a large RPZ zone that is
# changing frequently. For RPZ we pick names when generating RP query data
# file.

echo_t "INFO: starting latency monitoring"
i=0
for name in $QUERY_NAMES
do
    i=$((i + 1))
    $QA_STRESSDIR/monitor-latency.sh $name $ADDRESS4 $PORT \
        > $OUTDIR/latency${i}.txt 2>&1 &
    echo $! > $OUTDIR/latency${i}.pid
    echo_t "INFO: latency${i}.txt is the latency measurement file for $name"
    PID_FILES="$OUTDIR/latency${i}.pid $PID_FILES"
    export PID_FILES
done

# Start the process issuing reloads to the server under test if
# requested.
if [ -n "$RELOAD_INTERVAL" ]; then
    test $RELOAD_INTERVAL -ge 0 2> /dev/null
    if [[ $? -eq 0 ]]; then
        start_reload $NSNUM $RELOAD_INTERVAL
    fi
fi

# Now monitor, waking up every minute to check if things are OK.
to_go=$RUN_TIME
while [[ $to_go -gt 0 ]]
do
    echo_t "INFO: checking processes, $(convert_minutes $to_go) left"
    check_all_processes
    sleep 60
    to_go=$((to_go - 1))
done
echo_t "INFO: run finished"


# Run finished, get statistics on the various nameservers in case we need to
# do some diagnostics.  "server_output_dirs" was set earlier, when we started
# the servers.
echo_t "INFO: listing server statistics"
for dir in $server_output_dirs
do
    server=$(basename $dir)
    $RNDC -c $dir/rndc.conf stats
    $RNDC -c $dir/rndc.conf status
    echo_t "INFO: statistics for $server"
    cat $OUTDIR/$server/named.stats
done

result=0

# Terminate the processes
echo_t "INFO: killing all processes"
kill_all_processes

# Run the virtual memory analysis
echo_t "INFO: running analysis of virtual memory statistics"
$QA_STRESSDIR/analyze-stats.sh $server_output_dirs
ret=$?
result=$((result + ret))

echo_t "INFO: plot memory and latency graphs"
$QA_STRESSDIR/util/plot-graphs.sh
ret=$?
result=$((result + ret))

# For TCP, flamethrower defaults to using 30 traffic generators, each of
# which sends at most 100 queries every second.  This means that, in
# total, each flamethrower instance will not send more than 3000 TCP
# queries per second unless -c and/or -q are tweaked, even if -Q is set
# to a value larger than 3000.
if [[ "${RATE}" -gt 3000 ]]; then
    tcp_rate=3000
else
    tcp_rate="${RATE}"
fi

ns="ns${NSNUM}"
EXPECTED_TCP_RESPONSE_RATE=${EXPECTED_TCP_RESPONSE_RATE:-90}
# Check that no less than EXPECTED_TCP_RESPONSE_RATE % of the expected number
# of DoH, DoT, or TCP queries were received by the server.
if [[ "$USE_TCP" -gt 0 ]]; then
    # BIND 9.20 and older reports DoH and DoT queries as "TCP", newer versions
    # as "DoH" and "DoT".
    BIND_VERSION_MINOR=$("$NAMED" -v | awk -F. '{ print $2 }')
    if [[ "$BIND_VERSION_MINOR" -gt 20 ]]; then
        if [[ "$PROTOCOL" =~ "doh" ]]; then
            non_udp_protocol_name="DoH"
        elif [[ "$PROTOCOL" =~ "dot" ]]; then
            non_udp_protocol_name="DoT"
        else
            non_udp_protocol_name="TCP"
        fi
    else
        non_udp_protocol_name="TCP"
    fi
    tcp_queries_received=$(awk '/'${non_udp_protocol_name}' queries received/ { print $1 }' "${OUTDIR}/${ns}/named.stats")
    echo_t "INFO: Server '${ns}' (${MODE}) received $(group_digits "${tcp_queries_received:-an unknown number of}") ${non_udp_protocol_name} queries"
    tcp_queries_received_expected=$((2 * tcp_rate * RUN_TIME * 60))
    echo_t "INFO: About $(group_digits ${tcp_queries_received_expected}) ${non_udp_protocol_name} queries were expected"
    tcp_queries_received_minimum=$((tcp_queries_received_expected * EXPECTED_TCP_RESPONSE_RATE / 100))
    echo_t "INFO: Minimum number of ${non_udp_protocol_name} queries required to pass is $(group_digits ${tcp_queries_received_minimum})"
    if [[ "${tcp_queries_received:-0}" -ge "${tcp_queries_received_minimum}" ]]; then
        echo_t "INFO: BIND processed enough ${non_udp_protocol_name} queries"
        ret=0
    else
        echo_t "ERROR: BIND did not process enough ${non_udp_protocol_name} queries"
        ret=1
    fi
    result=$((result + ret))
fi

queries_received=0
qps_tcp_max=0
qps_udp_max=0
if [[ "$USE_TCP" -gt 0 ]]; then
    queries_received=$((queries_received + tcp_queries_received))
    # Theoretical limit of processed TCP queries. Two TCP query generators are used, each of which is limited to the lower of RATE and (30 * 100) queries per second.
    qps_tcp_max=$((2 * tcp_rate))
fi
if [[ "$USE_UDP" -gt 0 ]]; then
    queries_received=$((queries_received + $(awk '/UDP queries received/ { print $1 }' "${OUTDIR}/${ns}/named.stats" | tail -n 1)))
    # Theoretical limit of processed UDP queries. Two UDP query generators are used, each of which is limited to RATE queries per second.
    qps_udp_max=$((2 * RATE))
fi
qps=$((queries_received / (RUN_TIME * 60)))
echo_t "INFO: Tested ${MODE} server at the rate of about $(group_digits ${qps}) queries/second for ${RUN_TIME} minute(s)."
# Set received queries threshold to 60% of the theoretical limit.
qps_threshold=$(((qps_tcp_max + qps_udp_max) * 60 / 100))
if [[ "${qps}" -lt "${qps_threshold}" ]]; then
    echo_t "ERROR: The rate of $(group_digits ${qps}) queries is less than the threshold of $(group_digits ${qps_threshold}); this is suspiciously low."
    result=$((result + 1))
else
    echo_t "INFO: The queries-per-second threshold for passing this test was $(group_digits ${qps_threshold})."
fi

generate_backtrace

exit $result
