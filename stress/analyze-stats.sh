#!/usr/bin/env bash
set +xe
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

# analyze-stats.sh - Analyze Statistics Files
#
# Analyzes the files containing virtual memory statistics.
#
# The files are repeating lines of the form:
#
#       time    VM_Size
#
# The analysis is crudely simple.  It assumes that by half-way through the
# run, the process has reached maximum virtual memory size.  It compares this
# to the virtual size at the end of the run: if that is more that 0.5% greater,
# it terminates with an error.
#
# The virtual set size is computed by averaging 30 consecutive measurements.
# With the measurements being taken at 30-second intervals, this corresponds
# to an average over 15 minutres.
#
# Results are written to the file "result.txt" in the named directory.

. $QA_STRESSDIR/config.sh

# Perform the average for statistics in a given directory.
#
# The file "vm.txt" is assumed to be in the directory
#
# $1 - Name of the directory

average() {
    # Input file must be present
    if [ ! -e $1/vm.txt ]; then
        echo_t "ERROR: did not find a vm.txt in $1"
        exit 1
    fi

    # Work out where to start the analysis.
    lines=$(wc -l $1/vm.txt | awk '{print $1}')
    if [ $lines -lt 61 ]; then
        echo_t "WARNING: statistics file too short for analysis"
        exit 0
    fi

    echo_t "INFO: analysis for directory $1"
    awk -v MAX_LINES=$lines -v MAX_END_TO_MID_MEMORY_USE_RATIO=1.2 '\
        BEGIN { \
                mid_start = int(MAX_LINES / 2) - 30; \
                mid_end = mid_start + 29; \
                mid_sum = 0; \
                mid_min = 1000000000; \
                mid_max = 0;
                end_start = MAX_LINES - 30; \
                end_end = end_start + 29; \
                end_sum = 0; \
                end_min = 1000000000; \
                end_max = 0;
            } \
        NR >= mid_start && NR <= mid_end { \
                mid_sum += $NF; \
                if ($NF < mid_min) mid_min = $NF; \
                if ($NF > mid_max) mid_max = $NF; \
            } \
        NR >= end_start && NR <= end_end { \
                end_sum += $NF; \
                if ($NF < end_min) end_min = $NF; \
                if ($NF > end_max) end_max = $NF; \
            } \
        END { \
            mid_ave = mid_sum / 30; \
            end_ave = end_sum / 30; \
            print "Total of " NR " measurements"; \
            print "Mid point of run:"; \
            printf("   Minimum: %\047d KiB\n", mid_min); \
            printf("   Maximum: %\047d KiB\n", mid_max); \
            printf("   Average: %\047d KiB\n", mid_ave); \
            print "End of run:"; \
            printf("   Minimum: %\047d KiB\n", end_min); \
            printf("   Maximum: %\047d KiB\n", end_max); \
            printf("   Average: %\047d KiB\n", end_ave); \
            if (end_ave > MAX_END_TO_MID_MEMORY_USE_RATIO * mid_ave) { \
                print "ERROR: average end VM size is greater than " MAX_END_TO_MID_MEMORY_USE_RATIO " * average mid VM size"; \
                exit 2; \
            } \
        }' $1/vm.txt > $1/result.txt
    status=$?
    cat $1/result.txt
    if [ $status -ne 0 ]; then
        echo_t "ERROR: analysis job exited with error"
    fi
    return $status
}

# Get statistics for all directories passed as arguments
errcount=0
for directory in "$@" ; do
    average $directory
    errcount=$((errcount + $?))
done

if [ $errcount -eq 0 ]; then
    echo_t "INFO: no server displayed indications of large memory leak"
else
    echo_t "ERROR: possible memory leak in one or more servers"
fi

exit $errcount
