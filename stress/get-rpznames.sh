#!/usr/bin/env bash
#
# Get Some Names in RP Zones
#
# Used in testing RPZ, this script interrogates a series of RPZ zones and
# outputs the list of names that are being intercepted to stdout.

. "$QA_STRESSDIR/config.sh"

DIG="${DIG:-dig}"

# Lab RPZ distribution server
SERVER=rpzserver.isc.org

zones="adware.host.dtq. \
badrep.edit.host.dtq. \
bogons.ip.dtq. \
botnet.edit.host.dtq. \
botnetcc.host.dtq. \
botnetcc.ip.dtq. \
drop.ip.dtq. \
malware.host.dtq. \
phish.edit.host.dtq."

# Get the data
for zone in $zones; do
    # List the contents of the zone and:
    # a) Remove comments
    # b) Remove any blank lines
    # c) Extract the domain name
    # d) Remove the RP zone name suffix
    # e) Remove any resulting blank lines
    # ... to give a list of forbidden names
    "${DIG}" +onesoa "@$SERVER" "$zone" AXFR | \
        tee dig.rpz.txt | \
        grep -v '^;' | \
        grep -v '^[[:blank:]]*$' | \
        awk '{print $1}' | \
        sed -e "s/$zone\$//" | \
        grep -v '^[[:blank:]]*$' >> "$OUTDIR/rpz_names.sorted"
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        echo_t "ERROR: Command 'dig +onesoa @$SERVER $zone AXFR' failed:"
        cat dig.rpz.txt
        echo_t "ERROR: Make sure you can access $SERVER RPZ relay system (VPN may be needed)"
        exit 1
    fi
done
rm dig.rpz.txt
