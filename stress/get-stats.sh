#!/usr/bin/env bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

# get_stats.sh - Get Statistics
#
# Runs once a minute while the the stress test is running and gets the
# virtual memory size of the BIND process.
#
# The script is given the working directories of the BIND processes.  It
# assumes that these hold a "named.pid" file giving the PID of the process.
# The date/time and virtual memory size of the process is written to a
# vm.txt file in the same directory at minute intervals.
#
# Arguments:
# $1, $2 ...    Directories holding the PID files of the BIND processes.

. $QA_STRESSDIR/config.sh

# Do a precheck.
for directory in "$@" ; do
    if [ ! -e $directory/named.pid ]; then
        echo_t "ERROR: can't find named.pid in $directory"
        exit 1
    fi
    rm -f $directory/vm.txt
done

# Continue getting stats every 30 seconds until the process is killed.
while true ; do
    for directory in "$@" ; do
        pid=$(cat $directory/named.pid)
        if [[ "$(uname -s)" = "Linux" ]]; then
            mem=$(pmap --extended "$pid" | awk 'END { print $(NF-2)" "$(NF-1)" "$NF }')
        else
            mem=$(ps -p "$pid" -o vsz,rss | tail -n 1)
        fi
        echo_t "$mem" >> $directory/vm.txt
    done
    sleep 30
done
