#!/usr/bin/env bash
#
# collect-results.sh - Collects Output into tarball
#
# Collects the useful output in the "output" directory into a results directory
# tree, and from it creates a tarball.  That tarball is preserved as an
# artifact of the job.

set +e
. $(dirname $0)/config.sh

cd $WORKSPACE

# Delete files we don't need from the output tree. In particular, journal
# files and large zone files.

echo_t "INFO: deleting unwanted files from output directory tree"
find output -name '*.jnl'    -print -delete
find output -name '*.db'     -print -delete
find output -name '*.signed' -print -delete

# Spamhaus RP zone files and local RP zone files
find output -name '*.dtq'    -print -delete
find output -name '*.local'  -print -delete

tarball="results-$(date '+%Y-%m-%d-%H-%M').tar.gz"
echo_t "INFO: archiving results to $tarball"
tar cvfz $tarball output

exit 0
