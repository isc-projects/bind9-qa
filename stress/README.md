# Introduction
This directory hold scripts used to stress test BIND.  It starts BIND and
then uses the "flamethrower" package to send queries to it.  The virtual
memory usage is monitored through the duration of the job.

The files can test the following configuration, the configuration tested
depending on the setting of the MODE configuration option:

1. An authoritative server (MODE = authoritative).
2. A recursive server (MODE = recursive).
3. A recursive server that is accepting RPZ transfers from Spamhaus
   (MODE = rpz).

The last option was added in late 2019 for an investigation into performance
issues during RPZ transfers - see
https://wiki.isc.org/bin/view/Main/QueryLatencyTransferEffect.


When a job is run, it starts several instances of BIND:

* ns1: A "root server", delegating everything to the authoritative server.  This
is always started.

* ns2: An authoritative server that holds all the records for the test.  By
default this holds 100,000 records, something that can be changed by
setting the ZONE_SIZE configuration option.

* ns3: A recursive server.  This is configured with few options and will query
the authoritative server.  It is only started if MODE = recursive.

* ns4: A recursive server configured to received RPZ transfers from Spamhaus.
It is only started if MODE = rpz.

Once BIND is running, instances of "flamethrower" are started, sending their
queries to ns2 (if MODE = authoritative), ns3 (if MODE = recursive) or ns4
(if MODE = rpz).

Also started are:

1. A background process that every 30 seconds does a "ps" on the nameserver
   processes, recording both the virtual memory size (vsz) and the size of the
   resident set (rss).
2. Processes that once a second does a "dig" against the server being stressed.
   The time recorded by "dig" for the server to respond is recorded. The
   number of processes created depends on the number of names being queried
   for.

After the designated amount of time has passed, the job exits, killing the
processes it has started.  It then runs a simple analysis on the virtual memory
sizes: in particular, it looks at the previous two hours and sees if there has
been an increase in virtual memory usage greater than a particular threshold.

# Running the Stress Test

## Environment Variables
The stress test is run from the script "stress.sh", which is controlled by
environment variables.

A certain number of these are set in the configuration file "config.sh".  These
are ones that are unlikely to change between runs, although the settings of
some can be overridden by an external definition.  The variables are described
in "config.sh"

The remaining ones need to be defined before "stress.sh" is run.  If the script
is being run from Jenkins, these these are set either by Jenkins prompting for
their values before the script is run, by definitions in the Jenkins job
configuration screen, or both (some variables prompted for and some set in the
configuration screen).  These are documented in the header of "stress.sh".

## Directories
By default, the job assumes/uses the following directories:

**BIND_INSTALL_PATH**  
The version of BIND being tested is assumed to have been installed into
directory pointed to by this environment variable.

**OUTDIR**  
The job creates a directory whose name is given by this environment variable to
hold its output files.

If these are not defined externally, they are defined in "config.sh" to be
$WORKSPACE/bind9-install and $WORKSPACE/output respectively.  WORKSPACE is
environment variable which is not defined in these scripts: if the script is
run by Jenkins, WORKSPACE will be defined by Jenkins to be the working
directory it has created for the job.)

## Results
The following files are produced by the run:

**$OUTDIR/nsN/vm.txt**  
A file called "vm.txt" is placed in each nameserver's working directory, where
each file line holds three or four fields (depending on if the platform reports
dirty pages, at the moment, only Linux does):

    <time> <vsz> <rss> [<dirty>]

Where:

* \<time\> is the time the measurement was taken.
* \<vsz\> is the virtual memory size (in KiB)
* \<rss\> is the resident set size (in KiB)
* \<dirty\> is the dirty pages size (in KiB, where available)

**$OUTDIR/latencyN.txt**  
For each query name, a file called latencyN.txt is created, where N = 1 for the
first name in the list, 2 for the second name etc.  Each line of the file
contains:

    <epoch> <latency> <when>

where:

* \<epoch\> is the time at which the "dig" command used to measure the latency
returned from the query, expressed in seconds since 00:00, 1 January 1970.
* \<latency\> is the time recorded by "dig" for the query.  If the query timed
out, the figure will be 654321.
* \<when\> is the walue of <epoch> printed in a more readable format.


# Files
These are the files used during a run of the stress test.

**analyze-stats.sh**  
Script to analyze the memory file and see whether the virtual memory of
a process has been growing.

**bind9-stress-branchtag.jenkinsfile**  
Jenkins file holding the pipeline to test an arbitrary branch or tag from
either the public or private repository.  It is run by the Jenkins job
bind9-parameterized-stress-branchtag.

**bind9-stress-tarall.jenkinsfile**  
Jenkins file holding the pipeline to test an arbitrary tarball.  It is run by
the Jenkins job bind9-parameterized-stress-tarall.

**bind9-stress.jenkinsfile**  
Jenkins file containing the pipeline used to run the bind9-stress job. This
checks out the 'main' branch of the private repository and runs the stress
test on it, using the default options (which means that the test is run against
the recursive instance of BIND, with both UDP and TCP packet generators).

This is not strictly needed, as a job in Jenkins could be set up to
start bind9-parameterized-stress-branchtag with the appropriate parameters.
But in that case, the output would appear in as
bind9-parameterized-stress-branchtag output, which might be misleading.

**config.sh**  
File holding configuration settings for the stress job, typically for
parameters that are unlikely to be changed at run-time.

**get-rpznames.sh**  
A script used in the "rpz" test to get a sample of names from the RP zones
to use in the query file.

**get_stats.sh**  
The script that runs in the background collecting virtual memory statistics.

**monitor-latency.sh**  
The script that collects query latency information.

**stress.sh**  
The script that runs the stress job.

# Utility Programs/Files
The files in the "util" directory are used to generate/manipulate the zone
files.

**build-query-list.sh**  
A script for create a set of queries from a set of zone files.

**create-query-file.c**  
A program to create a query list.

**create_zone.c**  
Utility program for creating zones.  The program is compiled during the running
of the job to create the large zone file for ns2.

**create-query-file.c**  
Also built during the running of the script, this generates the query file used
by flamethrower.

**latency.gp**  
A Gnuplot script to plot the query latency.

**sign-zones.sh**  
Script to generate the signatures and trust-anchors configuration clause needed
for the zones in the test.

**make-trust-anchors.awk**  
Awk script used to convert a DNSKEY key file into a BIND "trust-anchors" clause.
It is used by sign-zones.sh.
