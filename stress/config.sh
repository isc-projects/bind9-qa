#!/usr/bin/env bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

# stress_config.sh - Set up Parameters for Stress Test
#
# This file is run by all shell scripts related to the BIND stress test.
# It defines the parameters for the job.
#
# Environment Variables Assumed Defined:
# WORKSPACE - points to the top-level directory of the Jenkins job.
#
# The QA repository is assumed to be checked out into the "qa" subdirectory
# of this.  Files relating to the stress test are in the "bind9/stress"
# subdirectory of that.
#
# The BIND repository is assumed to be checked out into the "bind9" subdirectory
# of $WORKSPACE.  BIND files are found in the normal places.

# Function to print message along with time stamp.  This is available to all
# scripts that include this one.
echo_t() {
    datetime=$(date "+%Y-%m-%d:%H:%M:%S")
    echo "$datetime $1"
}

# Location of the BIND installation.
BIND_INSTALL_PATH=${BIND_INSTALL_PATH:-$WORKSPACE/bind9-install}
export BIND_INSTALL_PATH

# Where all the output files are placed for later analysis
OUTDIR=${OUTDIR:-$WORKSPACE/output}
export OUTDIR

# Assign environment variables pointing to BIND programs.  Some, depending on
# the version of BIND, may be in the "bin" or "sbin" directory.
NAMED=$BIND_INSTALL_PATH/sbin/named
export NAMED

NAMED_CHECKCONF=$BIND_INSTALL_PATH/bin/named-checkconf
if [[ ! -e $NAMED_CHECKCONF ]]; then
    NAMED_CHECKCONF=$BIND_INSTALL_PATH/sbin/named-checkconf
fi
export NAMED_CHECKCONF

RNDC=$BIND_INSTALL_PATH/sbin/rndc
export RNDC

DIG=$BIND_INSTALL_PATH/bin/dig
if [[ ! -e $DIG ]]; then
    DIG=$BIND_INSTALL_PATH/sbin/dig
fi
export DIG

DNSSEC_SIGNZONE=$BIND_INSTALL_PATH/bin/dnssec-signzone
if [[ ! -e $DNSSEC_SIGNZONE ]]; then
    DNSSEC_SIGNZONE=$BIND_INSTALL_PATH/sbin/dnssec-signzone
fi
export DNSSEC_SIGNZONE

# Number of names in test.example zone served by ns2
ZONE_SIZE=${ZONE_SIZE:-100000}
export ZONE_SIZE

# OARC flamethrower
FLAME=${FLAME:-$HOME/opt/flame}
export FLAME

# C Compiler (used for generating the zone and query files)
CC=${CC:-gcc}
export CC
