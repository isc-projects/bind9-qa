#!/usr/bin/env bash
#
# reload.sh - Reload Configuration and Zone Files
#
# Called by the stress test, loops round constantly calling
# "rndc reload".
#
# Arguments:
#   $1  Name of the rndc configuration file to use to access the server. This
#       configuration file must include the TSIG key and the address/port
#       to which to connect.
#
#   $2  Interval between "rndc reload" commands.  If unspecified, there
#       is no gap.
#
# Note: the arguments are assumed to have been checked before this script
# is called.

# Get environment variables
. $(dirname $0)/config.sh

# Perform the tests
while true ; do
    $RNDC -c $1 reload
    if [ -n "$2" ]; then
        sleep $2
    fi
done
