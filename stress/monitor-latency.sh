#!/usr/bin/env bash
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.

# monitor-latency - Monitor Query Latency
#
# Runs once a second while the the stress test is running.  It queries for
# a name (using "dig") and records the time taken to a file for later
# analysis.
#
# monitor-latency name ns-address ns-port
#
# Arguments:
# $1        Name to query for.  This can include the type of record to query
#           by adding it (after a space) to the name, e.g. "isc.org NS".
# $2        Address of the nameserver to query.  This may be a V4 or V6
#           address.
# $3        Port on which to query.  If omitted, port 53 is used.
#
# Implicit Inputs
# DIG       If this environment variable is specified, it is the the "dig"
#           command to use.  Otherwise plain "dig" is used.
#
# Output:
# A stream of lines with the fields:
#
#       <epoch> <delay> <when>
#
# where:
#
# <epoch>   Date query was made in seconds since the start of the epoch.
# <delay>   Time taken for the query to complete, in milliseconds
# <when>    Value of <epoch> in a more readable format

if [ $# -lt 2 ] || [ $# -gt 3 ]
then
    echo "ERROR: Usage: monitor-latency.sh <name> <server> [port]"
    exit 1
fi

NAME="$1"
SERVER="$2"
PORT="${3:-53}"
DIG="${DIG:-dig}"

# Continue getting stats every second until the process is killed.
while true
do
    delay=$($DIG -p "$PORT" @"$SERVER" -q "$NAME" | awk '/Query time:/ { print $4 }')
    when=$(LC_TIME=C.UTF-8 date)
    if [ -z "$delay" ]; then
        delay=TIMEOUT
    fi
    if [[ "$(uname -s)" = "FreeBSD" ]]; then
        epoch=$(date -j -f "%a %b %d %T %Z %Y" "$when" +%s)
    else
        epoch=$(date --date="$when" +%s)
    fi
    echo "$epoch $delay $when"
    sleep 1
done
