#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Post a message to mattermost.
"""

import argparse
import os

import requests


def main():
    parser = argparse.ArgumentParser(description="Post message to Mattermost channel")
    parser.add_argument(
        "--channel", default="bind-9-automation", help="target mattermost channel"
    )
    parser.add_argument(
        "--text-file",
        nargs="+",
        type=argparse.FileType(encoding="UTF-8"),
        required=True,
        help="file with text to post",
    )
    args = parser.parse_args()

    webhook_url = os.getenv("MATTERMOST_WEBHOOK_URL")

    for f in args.text_file:
        payload = {
            "channel": args.channel,
            "text": f.read(),
        }
        response = requests.post(webhook_url, json=payload, timeout=60)
        response.raise_for_status()


if __name__ == "__main__":
    main()
