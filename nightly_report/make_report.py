#!/usr/bin/env python3
#
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Create a nightly report for the scheduled pipeline test runs.

https://gitlab.isc.org/isc-projects/bind9/-/pipeline_schedules
"""

import abc
from dataclasses import dataclass, field
import datetime
import os
from pathlib import Path

import gitlab
from jinja2 import Environment, FileSystemLoader


@dataclass(frozen=True)
class PipelineCheck:
    name: str
    project: str

    @abc.abstractmethod
    def get_last_pipeline(self, projects):
        pass

    @property
    @abc.abstractmethod
    def recent_results_expected(self):
        pass

    def get_last_pipeline_data(self, projects):
        pipeline = self.get_last_pipeline(projects)
        if not pipeline:
            return None

        pipeline_data = {
            "pipeline_id": pipeline.id,
            "proj_name": self.project,
        }

        if pipeline.started_at is None:  # may happen e.g. due to yaml error
            return pipeline_data | {
                "status_icon": "💥",
                "error": "Pipeline wasn't started - check pipeline for errors.",
            }

        # sanity check - pipeline was executed in last 24 hours
        pipe_time = datetime.datetime.fromisoformat(
            pipeline.started_at.replace("Z", "+00:00")
        )
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        if now - pipe_time > datetime.timedelta(hours=24):
            if not self.recent_results_expected:
                return None

            return pipeline_data | {
                "status_icon": "⏰",
                "error": (
                    "Pipeline wasn't executed tonight. "
                    f"Last run on **{pipe_time.strftime('%a %Y-%m-%d %H:%M')}**."
                ),
            }

        jobs = pipeline.jobs.list(get_all=True)
        if not jobs:
            return None

        project = projects[self.project]
        for bridge in pipeline.bridges.list(get_all=True):
            if not bridge.downstream_pipeline:
                continue
            child_pipeline = project.pipelines.get(bridge.downstream_pipeline["id"])
            jobs.extend(child_pipeline.jobs.list(get_all=True))

        failed_jobs = [
            job for job in jobs if job.status == "failed" and not job.allow_failure
        ]
        warning_jobs = [
            job for job in jobs if job.status == "failed" and job.allow_failure
        ]

        icon = "⚙️"
        if failed_jobs:
            icon = "❌"
        elif warning_jobs:
            icon = "⚠️"
        elif pipeline.status == "success":
            icon = "✅"

        return pipeline_data | {
            "status_icon": icon,
            "failed_jobs": failed_jobs,
            "warning_jobs": warning_jobs,
        }


@dataclass(frozen=True)
class ScheduledPipelineCheck(PipelineCheck):
    schedule_id: int
    recent_results_expected: bool = field(default=True)

    def get_last_pipeline(self, projects):
        project = projects[self.project]
        schedule = project.pipelineschedules.get(self.schedule_id)
        pipeline_id = schedule.last_pipeline["id"]
        return project.pipelines.get(pipeline_id)


@dataclass(frozen=True)
class BranchPipelineCheck(PipelineCheck):
    branch: str
    recent_results_expected: bool = field(default=False)

    def get_branch_name(self, _):
        return self.branch

    def get_last_pipeline(self, projects):
        project = projects[self.project]
        branch = self.get_branch_name(projects)
        if branch is None:
            return None

        pipelines = project.pipelines.list(
            ref=branch, order_by="id", sort="desc", iterator=True
        )
        try:
            pipeline_id = next(pipelines).id
        except StopIteration:
            return None
        return project.pipelines.get(pipeline_id)


@dataclass(frozen=True)
class BranchRegexPipelineCheck(BranchPipelineCheck):

    def get_branch_name(self, projects):
        project = projects[self.project]
        branches = project.branches.list(regex=self.branch, sort="name_asc", all=True)
        return branches[-1].name if branches else None


PIPELINE_CHECKS = {
    "9.21": (
        ScheduledPipelineCheck(
            name="main", project="isc-projects/bind9", schedule_id=3
        ),
        ScheduledPipelineCheck(
            name="main (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=72,
        ),
        BranchPipelineCheck(
            name="main-PRE-RELEASE",
            project="isc-private/bind9",
            branch="main-PRE-RELEASE",
        ),
        BranchRegexPipelineCheck(
            name="v9.21.z-release",
            project="isc-private/bind9",
            branch="v9.21.[0-9]+-release-PRE-RELEASE",
        ),
    ),
    "9.20": (
        ScheduledPipelineCheck(
            name="bind-9.20", project="isc-projects/bind9", schedule_id=86
        ),
        ScheduledPipelineCheck(
            name="bind-9.20 (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=85,
        ),
        BranchPipelineCheck(
            name="bind-9.20-PRE-RELEASE",
            project="isc-private/bind9",
            branch="bind-9.20-PRE-RELEASE",
        ),
        BranchRegexPipelineCheck(
            name="v9.20.z-release",
            project="isc-private/bind9",
            branch="v9.20.[0-9]+-release-PRE-RELEASE",
        ),
    ),
    "9.18": (
        ScheduledPipelineCheck(
            name="bind-9.18", project="isc-projects/bind9", schedule_id=36
        ),
        ScheduledPipelineCheck(
            name="bind-9.18 (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=73,
        ),
        BranchPipelineCheck(
            name="bind-9.18-PRE-RELEASE",
            project="isc-private/bind9",
            branch="bind-9.18-PRE-RELEASE",
        ),
        BranchRegexPipelineCheck(
            name="v9.18.z-release",
            project="isc-private/bind9",
            branch="v9.18.[0-9]+-release-PRE-RELEASE",
        ),
    ),
    "9.18-S": (
        ScheduledPipelineCheck(
            name="bind-9.18-sub", project="isc-private/bind9", schedule_id=46
        ),
        ScheduledPipelineCheck(
            name="bind-9.18-sub (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=75,
        ),
        BranchPipelineCheck(
            name="bind-9.18-sub-PRE-RELEASE",
            project="isc-private/bind9",
            branch="bind-9.18-sub-PRE-RELEASE",
        ),
        BranchRegexPipelineCheck(
            name="v9.18.z-S-release",
            project="isc-private/bind9",
            branch="v9.18.[0-9]+-S[0-9]+-release-PRE-RELEASE",
        ),
    ),
    "9.16-S": (
        ScheduledPipelineCheck(
            name="bind-9.16-sub (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=76,
        ),
        BranchPipelineCheck(
            name="bind-9.16-sub-PRE-RELEASE",
            project="isc-private/bind9",
            branch="bind-9.16-sub-PRE-RELEASE",
        ),
    ),
    "9.11-S": (
        ScheduledPipelineCheck(
            name="bind-9.11-sub (pre-release merge)",
            project="isc-private/bind9",
            schedule_id=77,
        ),
        BranchPipelineCheck(
            name="bind-9.11-sub-PRE-RELEASE",
            project="isc-private/bind9",
            branch="bind-9.11-sub-PRE-RELEASE",
        ),
    ),
}


def export_template(out_name, data):
    j2_env = Environment(loader=FileSystemLoader(str(Path(__file__).resolve().parent)))

    template = j2_env.get_template("nightly_report.md.j2")
    with open(out_name, "w", encoding="utf-8") as f:
        f.write(template.render(data))


def main():
    gl_url = os.getenv("CI_SERVER_URL", None)
    if gl_url is not None:
        token = os.getenv("BIND_TEAM_API_TOKEN")
        gl = gitlab.Gitlab(gl_url, token)
    else:  # running locally on dev machine
        gl = gitlab.Gitlab.from_config()

    projects = {
        "isc-projects/bind9": gl.projects.get("isc-projects/bind9", lazy=True),
        "isc-private/bind9": gl.projects.get("isc-private/bind9", lazy=True),
    }

    today = datetime.date.today()
    today_str = today.strftime("%a %Y-%m-%d")

    for version, checks in PIPELINE_CHECKS.items():
        data = {
            "day": today_str,
            "version": version,
            "checks": {},
        }

        for pipeline_check in checks:
            pipeline_data = pipeline_check.get_last_pipeline_data(projects)
            if pipeline_data is not None:
                data["checks"][pipeline_check.name] = pipeline_data

        export_template(
            f"nightly_report_{today.strftime('%Y-%m-%d')}_{version}.md", data
        )


if __name__ == "__main__":
    main()
