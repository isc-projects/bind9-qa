# Response Differencing

## Introduction

respdiff is a set of programs written by CZ.NIC to send queries to different DNS
servers and to compare the responses. As the answers can be different, it allows
configuration of the comparison function to exclude the answers and only check
things like flags, etc. (respdiff can be found at
https://gitlab.nic.cz/knot/respdiff).

The files in this directory form an instrumentation project that can run BIND
or third-party vendor's servers configured as a recursive server through a
"respdiff" test. A reference version of either BIND, or Knot Resolver, Unbound,
and PowerDNS Recursor are selected, as well as the version of BIND under test.
Reference servers and test versions of BIND are expected to be present in the
environment when the respdiff.sh script is started.

The script configures respdiff to start one test nameserver and multiple copies
of the reference version. Multiple reference nameservers are a good idea when
querying for a name; it is quite possible to get different answers to repeated
queries. With multiple reference nameservers, respdiff omits from statistics
queries for which the reference nameservers do not obtain the same answer. For
those where all reference nameservers agree, it reports queries where the answer
received from the test nameserver differs.

## Runtime Details

* The reference versions of resolver are started on 10.53.0.N, where N is higher
  than 1 and lower than 10.
* The test version of BIND is started on 10.53.0.10.

Before starting the run of respdiff, the latest version of the software is
obtained from the CZ.NIC git repository. The repository is cloned if it doesn't
exist, or the local copy (in $HOME/respdiff) is updated.

The configuration files for the run are created, the nameservers started, and
respdiff is allowed to run. When completed, the resolver instances are stopped.
If the number of mismatches exceeds a threshold, the job is assumed to have
failed.

## Files

**100k_mixed.txt, 10k_a.txt, 20a.txt**

Files containing queries that respdiff can use. By default the 100k_mixed.txt
file is used.

**ref-named.conf.in, ref-unbound.conf.in, test-named.conf.in**

Configuration templates for the reference and test instances. They are
processed before use to set the correct address and port (and, in the case of
the test instance, any supplied configuration file options).

**refserverN.cfg**

Configuration files for the different reference instances.

**respdiff.cfg.in**

respdiff configuration file. This is processed to set specific variables before
being used.

**respdiff.sh**

Script that actually runs the respdiff test.

## 100k_mixed.txt query set

The source of queries for this dataset comes from
data.isc.org/bind9-resolver-benchmark/telco-eu-2023-03/01.pcap (in AWS S3).

The list of queries was reduced by
[filter_unique.lua](https://gitlab.nic.cz/knot/respdiff/-/blob/master/contrib/dataset_filter/filter_unique.lua)
to limit the number of queries to the same domain. Afterwards, queries with
certain qtypes (ANY and non-standardized ones) plus incorrectly-decoded labels
were manually pruned from the list. The result was trimmed to 100k queries.
