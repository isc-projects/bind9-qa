#!/bin/bash
#
# respdiff.sh - Run "respdiff" on the specified version of BIND
#

set -e
set -o nounset

# Constants

TESTIP="10.53.0.10"
TESTPORT="5300"

REFIP_ROOT="10.53.0."
REFPORT="5300"

# URL of the repository holding the respdiff code
RESPDIFF_URL=https://gitlab.nic.cz/knot/respdiff.git

# The maximum allowed ratio of non-timeout disagreements between the responses
# returned by the reference BIND version (or other DNS server implementation)
# and the responses returned by the tested BIND version, expressed as a
# percentage (the default is 0.1%).  If this threshold is exceeded, the script
# will return a non-zero exit code.
MAX_DISAGREEMENTS_PERCENTAGE=${MAX_DISAGREEMENTS_PERCENTAGE:-0.1}

# Functions

usage() {
    cat << EOF
Usage:
  respdiff.sh [-c refcount] [-h] [-o options] [-m path] [-q file] [-r respdir]
              [-s ref_server_type] [-w workdir] testdir [refdir]

Arguments:
-c refcount Number of instances of the reference implementation to run.
            By default, one reference server is run. The maximum reference
            servers configured is 9 for "named", if "-s third_party" is set
            tripletts of Knot Resolver, Unbound, and PowerDNS Recursor are
            used instead and the "refcount" has to be between 1 and 3.

-h          Print this text and exit.

-m path     Check for memory leaks by LD_PRELOADing the jemalloc shared library
            provided in "path" and enabling heap profiling for the tested named
            instance.

-q file     File to use for the queries. If not present, the file
            "respdiff-queries.txt" in the directory holding this script is
            used.
         
-r respdir  Local git repository holding the "respdiff" code.  If not present,
            \$HOME/respdiff is assumed.  The repository is cloned if it
            doesn't exist and is updated before each run.

-s ref_server_type
            Name of the reference server implementation. Either "named"
            or "third_party" are suported. "third_party" name starts an
            instance of Knot Resolver, Unbound, and PowerDNS Recursor
            per number of instances defined by the "-c" option.
            E.g. "-c 1" starts in total three third-party reference
            servers (one per vendor), "-c 3" starts nine servers in total
            (three reference servers of each vendor). This option does not
            influence the test server type. Default to "named".

-w workdir  Directory in which respdiff will create its various files. If
            not defined, \$HOME/rpsworkdir will be used.  This directory
            will is created for the run and must not exist beforehand.

testdir     Directory holding the version of BIND under test.
            (Use build directory, not the install directory.)

refdir      Directory holding the reference version of BIND. The test
            version is being compared agains this version of BIND. This is
            required only when the "-s" is set to "named".
EOF
}

# Message functions

errmsg() {
    echo "*** ERROR: $*"
}

infomsg() {
    echo "*** INFO: $*"
}

# Print error message and exit.
#
# Invocation:
#   fatal [-u] "Message"
#
# If -u is specified, the usage message is also printed.

fatal() {
    print_usage=0
    if [ "$1" = "-u" ]; then
        print_usage=1
        shift
    fi
    errmsg "$1"
    if [ $print_usage -eq 1 ]; then
        usage
    fi
    exit 1
}

terminate_server_pids() {
    local status=0
    local termopt="${1}"
    local returnstatus="${2:-}"
    for pid in "${server_pids[@]}"; do
        if kill -0 "$pid" 2>/dev/null; then
            if [ "${returnstatus}" = "returnstatus" ]; then
                infomsg "terminating server process ${pid}: $(ps --no-headers -o 'cmd' --pid "${pid}")"
                status=$((status + 1))
                kill "$termopt" "$pid" || true
                if [ "${termopt}" = "-SIGABRT" ]; then
                    infomsg "Wait for core dumping of process ${pid} to finish"
                    sleep 60
                fi
            else
                kill "$termopt" "$pid" || true
            fi
        fi
    done
    return "${status}"
}

# This code is slightly suboptimal as we always sleep for some time even when
# there's nothing to be aborted after successful process termination. It can be
# solved by aborting processes without the termination "mid-step", but we prefer
# leeway for servers to terminate cleanly.
#
# There are race conditions as we may abort a process with PID, which belonged
# to a server process. This may be fixed by using "/usr/bin/kill --timeout",
# but Debian provides "kill" from the procps package, which does not have this
# option, contrary to "kill" from the util-linux package if Debian didn't
# disable building "kill" in util-linux.
server_cleanup() {
    infomsg "terminating server processes ${server_pids[*]}"
    terminate_server_pids "-SIGTERM"
    sleep 60
    infomsg "aborting remaining server processes (if any)"
    terminate_server_pids "-SIGABRT" "returnstatus" || status=$?
    if ! wait "${named_under_test_pid}"; then
        errmsg "named under test (PID ${named_under_test_pid}) terminated with non-zero exit status"
        status=$((status + 1))
    fi
}

trap server_cleanup ERR SIGINT SIGTERM

# Main Code

# Process the command line.
configdir=$(pwd)
queryfile=$configdir/respdiff-queries.txt
rspdir=$HOME/respdiff
workdir=$HOME/rspworkdir
refcount=1
jemalloc_path=
ref_server_type=named
server_pids=()
status=0

while getopts "c:hm:q:r:s:w:" flag; do
    case "$flag" in
        c) refcount=$OPTARG ;;
        h) usage ; exit 0 ;;
        m) jemalloc_path=$OPTARG ;;
        q) queryfile=$OPTARG ;;
        r) rspdir=$OPTARG ;;
        s) ref_server_type=$OPTARG ;;
        w) workdir=$OPTARG ;;
        *) fatal -u "Unknown option specified" ;;
    esac
done
shift $((OPTIND - 1))

# Get directory holding the miscellaneous configuration files.  This is the
# directory that holds this file.
pushd "$(dirname "$0")" > /dev/null || exit 1
popd > /dev/null || exit 1

# Only "named" and "third_party" recursor implementations are supported.
if [[ ! "${ref_server_type}" =~ named|third_party ]]; then
    fatal "Unknown server implementation: ${ref_server_type}"
fi

bindtest=${1:-}
if [ "$bindtest" = "" ]; then
    fatal -u "must specify the BIND-under-test build directory"
elif [ ! -d "$bindtest" ]; then
    fatal "directory $bindtest does not exist"
elif [ ! -x "$bindtest/bin/named/named" ]; then
    fatal "'bin/named/named' binary does not exist in $bindtest"
fi

# Set more meaningful names for the arguments and check that the directories
# exist. This is not needed when third-party reference servers are used.
if [ "${ref_server_type}" = "named" ]; then
    reference_named=${2:-}
    if [ ! -x "${reference_named}" ]; then
        fatal -u "reference named binary must be an existing executable"
    fi
fi

# Ensure that the "respdiff" repository is present, creating it if it doesn't
# exist.
if [ ! -d "$rspdir" ]; then
    infomsg "cloning respdiff repository from $RESPDIFF_URL"
    git clone "$RESPDIFF_URL" "$rspdir"
else
    infomsg "updating the respdiff repository"
    cd "$rspdir" || exit 1
    git clean -fdx
    git checkout master
    git pull
fi

# Check that the number of reference implementations is valid.
if ! test "$refcount" -eq "$refcount" 2> /dev/null; then
    fatal -u "value passed to -c must be an integer"
elif [ "$refcount" -lt 1 ]; then
    fatal "number of instances of test server must be between 1 and 9"
elif [ "${ref_server_type}" = "third_party" ] && [ "$refcount" -gt 3 ]; then
    fatal "number of instances of third-party test servers must be between 1 and 3"
elif [ "$refcount" -gt 9 ]; then
    fatal "number of instances of test server must be between 1 and 9"
fi

# Check that the query file exists.
if [ ! -e "$queryfile" ]; then
    fatal "file $queryfile not found"
fi

# Create the working directory.  To avoid problems with multiple runs,
# this must not exist before the script is run.
if [ -e "$workdir" ]; then
    fatal -u "directory $workdir already exists"
fi
mkdir "$workdir"

# The working directories for the BIND runs will be subdirectories of
# the working directory.
testdir="$workdir/testbind"
mkdir "$testdir"

refdir_root="$workdir/refserver"

# Create the configuration files for the runs.  These will be held
# in the working directories of each server.
infomsg "creating test config file"
TESTRUNOPT=${TESTRUNOPT:-}
sed -e "s?@DIRECTORY@?$testdir?"  \
    -e "s?@PORT@?$TESTPORT?" \
    -e "s?@IP@?$TESTIP?" \
    -e "s?@TESTRUNOPT@?$TESTRUNOPT?" \
    "$configdir/test-named.conf.in" > "$testdir/named.conf"

create_ref_config() {
    local server_type="$1"
    local server_count="$2"
    mkdir "${refdir_root}${server_count}"
    infomsg "creating config file for reference instance $server_count"
    sed -e "s?@DIRECTORY@?${refdir_root}${server_count}?"  \
        -e "s?@IP@?${REFIP_ROOT}${server_count}?" \
        -e "s?@PORT@?${REFPORT}?" \
        "$configdir/ref-${server_type}.conf.in" > "${refdir_root}${server_count}/${server_type}.conf"
}

if [ "${ref_server_type}" = "named" ]; then
    count=1
    while [ $count -le "$refcount" ]; do
        create_ref_config named "${count}"
        count=$((count + 1))
    done
elif [ "${ref_server_type}" = "third_party" ]; then
    count=1
    while [ $count -le "$refcount" ]; do
        scount=$((count * 3 - 2))
        create_ref_config kresd ${scount}
        create_ref_config unbound $((scount + 1))
        create_ref_config powerdns $((scount + 2))
        count=$((count + 1))
    done
fi

# Create the respdiff configuration file.  This resides in the directory
# holding the various programs.
infomsg "creating respdiff config file"
SYSTEMS="testbind"
# EDNS0 UDP size is different for PowerDNS Recursor, we should ignore the EDNS
# field to eliminate differences with BIND:
# https://doc.powerdns.com/recursor/appendices/FAQ.html.
edns=", edns"
if [ "${ref_server_type}" = "third_party" ]; then
    edns=
fi
sed -e "s/@EDNS@/$edns/" "${configdir}/respdiff.cfg.in" > "${workdir}/respdiff.cfg"

# Summarise the parameters before the test starts.
infomsg "summary of run configuration on $(date)"
infomsg "'respdiff' repository: $rspdir"
infomsg "working directory: $workdir"
infomsg "number of reference instances: $refcount"
infomsg "query file: $queryfile"

infomsg "test named version: $("$bindtest/bin/named/named" -v)"
infomsg "run directory: $testdir"
infomsg "associated configuration file:"
cat "$testdir/named.conf"

start_third_party_ref_server() {
    local ref_server_type="$1"
    local server_count="$2"
    local server_version="$3"
    infomsg "reference ${ref_server_type} (instance ${server_count}) version: ${server_version}"
    infomsg "associated configuration file:"
    cat "${refdir_root}${server_count}/${ref_server_type}.conf"
    infomsg "run directory: ${refdir_root}${server_count}"
    SYSTEMS="${SYSTEMS}, refserver${server_count}"
    cat "${configdir}/refserver${server_count}.cfg" >> "${workdir}/respdiff.cfg"
    infomsg "starting instance ${server_count} of the ${ref_server_type} reference server"
}

# Start the instances of reference server.
if [ "${ref_server_type}" = "named" ]; then
    count=1
    while [ $count -le "$refcount" ]; do
        infomsg "reference named (instance ${count}) version: $("${reference_named}" -v)"
        infomsg "associated configuration file:"
        directory=${refdir_root}${count}
        cat "${directory}/named.conf"
        infomsg "run directory: ${directory}"
        SYSTEMS="${SYSTEMS}, refserver${count}"
        cat "${configdir}/refserver${count}.cfg" >> "${workdir}/respdiff.cfg"
        infomsg "starting instance $count of the named reference server"
        "${reference_named}" -4 -g -c "${directory}/named.conf" > "${directory}/named.run" 2>&1 &
        server_pids+=($!)
        count=$((count + 1))
    done
elif [ "${ref_server_type}" = "third_party" ]; then
    # Obtain initial trust anchor for Unbound
    count=1
    while [ $count -le "$refcount" ]; do
        scount=$((count * 3 - 2))

        kresd_version="$(kresd -V | awk '{ print $4 }')"
        start_third_party_ref_server kresd "${scount}" "${kresd_version}"
        directory=${refdir_root}${scount}
        pushd "${directory}"
        kresd -c "${directory}/kresd.conf" -n > "${directory}/kresd.run" 2>&1 &
        server_pids+=($!)
        popd

        unbound_version="$(unbound -V | awk '{ print $2; exit }')"
        directory=${refdir_root}$((scount + 1))
        infomsg "creating initial trust anchor for Unbound"
        unbound-anchor -v -a "$directory/root.key" || true
        test -f "$directory/root.key"
        start_third_party_ref_server unbound "$((scount + 1))" "${unbound_version}"
        unbound -c "${directory}/unbound.conf" -d > "${directory}/unbound.run" 2>&1 &
        server_pids+=($!)

        powerdns_version="$(pdns_recursor --version 2>&1 | awk '/PowerDNS Recursor/ { print $6 }')"
        start_third_party_ref_server powerdns "$((scount + 2))" "${powerdns_version}"
        directory=${refdir_root}$((scount + 2))
        # PowerDNS Recursor configuration file has to be named "recursor.conf".
        mv "${directory}/powerdns.conf" "${directory}/recursor.conf"
        pdns_recursor --config-dir="${directory}" > "${directory}/powerdns.run" 2>&1 &
        server_pids+=($!)

        count=$((count + 1))
    done
fi

RESPDIFF_JOBS=${RESPDIFF_JOBS:-64}
sed -i \
    -e "s:@RESPDIFF_JOBS@:${RESPDIFF_JOBS}:" \
    -e "s:@SYSTEMS@:${SYSTEMS}:" "$workdir/respdiff.cfg"

infomsg "starting test version of BIND"
if [ -n "$jemalloc_path" ]; then
    LD_PRELOAD="$jemalloc_path" MALLOC_CONF="prof_leak:true,lg_prof_sample:0,prof_final:true" "$bindtest/bin/named/named" -4 -g -c "$testdir/named.conf" > "$testdir/named.run" 2>&1 &
    server_pids+=($!)
else
    "$bindtest/bin/named/named" -4 -g -c "$testdir/named.conf" > "$testdir/named.run" 2>&1 &
    server_pids+=($!)
fi
named_under_test_pid="${server_pids[-1]}"

# Run the various Python scripts.
cd "$rspdir" || exit 1

infomsg "preparing the query file: $queryfile"
./qprep.py "$workdir" < "$queryfile"
infomsg "executing queries"
./orchestrator.py --config "$workdir/respdiff.cfg" --ignore-timeout "$workdir"
infomsg "identifying differences"
./msgdiff.py --config "$workdir/respdiff.cfg" "$workdir"
infomsg "difference summary"
./diffsum.py --config "$workdir/respdiff.cfg" --limit 0 "$workdir" | tee "$workdir/report.txt"

# Terminate server instances. Give them time to shutdown, then really kill any
# that survive.
infomsg "shutting down server instances"

server_cleanup
trap - ERR SIGINT SIGTERM

get_from_report() {
    jq "$@" "${workdir}/report.json"
}

rounded_percentage() {
    printf "%.2f%%" "${1}"
}

disagreements="$(get_from_report ".target_disagreements.count")"
summary_usable_answers="$(get_from_report ".summary.usable_answers // 0")"
if [ "${summary_usable_answers}" -eq 0 ]; then
    disagreements_percentage=0
else
    disagreements_percentage="$(get_from_report "$disagreements / $summary_usable_answers * 100")"
fi
if [ -z "$(get_from_report ".summary.fields.timeout // empty")" ]; then
    timeouts=0
else
    timeouts="$(get_from_report "[.summary.fields.timeout.mismatches[].count] | add")"
fi
if [ "${disagreements}" -eq 0 ]; then
    timeouts_percentage=0
else
    timeouts_percentage="$(get_from_report "$timeouts / $disagreements * 100")"
fi

echo -n "Target disagreements between the tested version and the reference one "
if [ "${ref_server_type}" = "named" ]; then
    echo -n "($("${reference_named}" -v)) comprise $(rounded_percentage "$disagreements_percentage") of not ignored answers; "
elif [ "${ref_server_type}" = "third_party" ]; then
    echo -n "(Knot Resolver ${kresd_version}, Unbound ${unbound_version}, and PowerDNS Recursor ${powerdns_version}) "
    echo -n "comprise $(rounded_percentage "$disagreements_percentage") of not ignored answers; "
fi
echo "of these, $(rounded_percentage "${timeouts_percentage}") are timeout disagreements, which can be attributed to network issues."

non_timeout_disagreements=$((disagreements - timeouts))
max_disagreements="$(get_from_report ".total_queries * ${MAX_DISAGREEMENTS_PERCENTAGE} / 100 | floor")"

if [ "${non_timeout_disagreements}" -gt "${max_disagreements}" ]; then
    echo "ERROR: The number of non-timeout disagreements (${non_timeout_disagreements} queries) exceeds the configured threshold (${max_disagreements} queries)."
    status=1
fi

while read -r core; do
    backtrace_file="${core}-backtrace.txt"
    errmsg "Core dump file $core found"
    gdb -batch -ex "bt" -core "$core" -- "$bindtest/bin/named/.libs/named" 2>/dev/null | sed -n '/^Core was generated by/,$p'
    errmsg "Complete backtrace to be found in: $backtrace_file"
    gdb -batch -ex "thread apply all bt full" -core "$core" -- "$bindtest/bin/named/.libs/named" > "$backtrace_file" 2>&1
    status=$((status + 1))
done < <(find "$testdir" \( -name 'core' -or -name 'core.*' -or -name '*.core' \))

if [ -n "$jemalloc_path" ]; then
    HEAP_PROFILE="$(grep -l -F /bin/named/ "${testdir}"/jeprof.*.heap)"
    NAMED_BINARY="$(awk "/\/bin\/named\// { print \$NF; exit }" "${HEAP_PROFILE}")"
    infomsg "Processing heap profile in ${HEAP_PROFILE}"
    jeprof \
        --show_bytes \
        --nodefraction=0 \
        --ignore="__tzset|_dl_start_user|allocate_dtv|call_rcu_data_init|create_managers|isc_loopmgr_run|pthread_once|urcu_memb_unregister_thread" \
        "${NAMED_BINARY}" \
        "${HEAP_PROFILE}" \
        --pdf >jeprof.pdf 2>jeprof.err || :
    if grep -q "No nodes to print" jeprof.err; then
        rm -f jeprof.pdf jeprof.err
        infomsg "No memory leaks found"
    else
        mv jeprof.pdf jeprof.err "${bindtest}"
        errmsg "Memory leaks found, see ${bindtest}/jeprof.pdf"
        status=1
    fi
fi

exit "${status}"
